import { GenericStore } from './generic-store';
import { Injectable } from '@angular/core';
import { StoreModule } from '../store.module';
import { Producer } from '../../model/producer';
import { ParametersStore } from './parameters-store.service';
import Big from 'big.js';

/**
 * Gère les producteurs de la partie
 */
@Injectable({
  providedIn: StoreModule
})
export class ProducersStore extends GenericStore<Producer[]> {

  constructor (private parametersStore: ParametersStore) {
    super([]);
  }

  /**
   * Initialise la valeur du store
   * @param {Producer[]} producers
   */
  init (producers: Producer[]): void {
    this.setState(producers);
  }

  /**
   * Met à jour un producteur dans la liste
   *
   * @param {Producer} producer
   */
  update (producer: Producer): void {
    let value = this.value;
    value[producer.id] = producer;

    value = this.increaseAdditionalModeratorProd(value);
    this.setState(value);
  }

  /**
   *
   */
  private increaseAdditionalModeratorProd (producers: Producer[]): Producer[] {
    let n = 0;
    for (const producer of producers) {
      if (producer.id !== 0) {
        n += producer.number;
      }
    }

    producers[0].additionalProduction = this.parametersStore.value.addModeratorProd.mul(n);

    return producers;
  }
}
