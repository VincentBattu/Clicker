import { GenericStore } from './generic-store';
import { Injectable } from '@angular/core';
import { StoreModule } from '../store.module';
import { Upgrade } from '../../model/upgrades/upgrade';

@Injectable({
  providedIn: StoreModule
})
export class UpgradesStore extends GenericStore<Upgrade[]> {

  constructor () {
    super([]);
  }

  /**
   * Initialise la valeur du store
   * @param {Upgrade[]} upgrades
   */
  init (upgrades: Upgrade[]): void {
    upgrades.forEach((u, i) => u.id = i);
    this.setState(upgrades);
  }

  /**
   * Met à jour une amélioration dans la liste
   *
   * @param {Upgrade} upgrade
   */
  update (upgrade: Upgrade): void {
    const value = this.value;
    value[upgrade.id] = upgrade;

    this.setState(value);
  }
}
