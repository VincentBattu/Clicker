import { BehaviorSubject } from 'rxjs';

/**
 * Store générique
 */
export class GenericStore<T> {

  private _state$: BehaviorSubject<T>;

  /**
   * Constructeur
   * @param {T} initialState
   */
  public constructor (initialState: T) {
    this._state$ = new BehaviorSubject<T>(initialState);
  }

  /**
   * Récupère la dernière valeur
   * @returns {T}
   */
  public get value () {
    return this._state$.getValue();
  }

  /**
   * Récupère l'observable permettant de souscrire au changement du score
   * @returns {Observable<T>}
   */
  public get state () {
    return this._state$.asObservable();
  }

  /**
   * Met à jour la valeur du store. Les changements doivent être imutables !
   * @param {T} nextState
   */
  public setState (nextState: T): void {
    this._state$.next(nextState);
  }
}
