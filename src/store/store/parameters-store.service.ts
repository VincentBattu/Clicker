import { GenericStore } from './generic-store';
import { Injectable } from '@angular/core';
import Big from 'big.js';
import { StoreModule } from '../store.module';
import { Producer } from '../../model/producer';

/**
 * Paramètres partagés
 */
export interface Parameters {
  /**
   * Nom de la partie
   */
  gameName: string;

  /**
   * Possibilité de changer le nom de la partie
   */
  changeGameName: boolean;

  /**
   * Augmentation par clic
   */
  perClick: Big;

  /**
   * Augmentation par seconde
   */
  perSecond: Big;

  /**
   * Ajout de production à chaque modérateur
   */
  addModeratorProd: Big;
}

/**
 * Gère les paramètres de la partie
 */
@Injectable({
  providedIn: StoreModule
})
export class ParametersStore extends GenericStore<Parameters> {

  /**
   * Noms de partie utilisé pour commencer.
   * @type {any[]}
   */
  private static readonly GAME_NAMES = [
    'Ma super partie',
    'Tibo je t\'aime',
    'Tibo je te déteste',
    'Tibo ne dit surtout pas à Laëtitia ce qu\'on a fait hier soir'
  ];

  constructor () {

    // On choisit un nom de partie au hasard
    const gameName = ParametersStore.GAME_NAMES[Math.floor(Math.random() * ParametersStore.GAME_NAMES.length)];

    // On initialise les paramètres
    super({
      gameName,
      changeGameName: false,
      perClick: new Big(1),
      perSecond: new Big(0),
      addModeratorProd: new Big(0)
    });
  }

  /**
   * Permet de modifier le nom de la partie.
   * @param {string} gameName
   */
  public set gameName (gameName: string) {
    this.setState({
      ...this.value,
      gameName
    });
  }

  /**
   * Permet de modifier la possibilité de changer le nom de la partie
   * @param {boolean} changeGameName
   */
  public setChangeGameName (changeGameName: boolean) {
    this.setState({
      ...this.value,
      changeGameName
    });
  }

  /**
   * Calcule la production actuelle
   * @param {Producer[]} producers
   */
  public computePerSecond (producers: Producer[]) {
    let perSecond = new Big(0);

    for (const producer of producers) {
      perSecond = perSecond.add(producer.production.mul(producer.number));
    }

    this.setState({
      ...this.value,
      perSecond
    });
  }

  /**
   * Modifie la valeur du click
   * @param {Big} perClick
   */
  public setPerClick (perClick: Big) {
    this.setState({
      ...this.value,
      perClick
    });
  }

  /**
   * Modifie la production par seconde
   * @param {Big} perSecond
   */
  public set perSecond (perSecond: Big) {
    this.setState({
      ...this.value,
      perSecond
    });
  }

  /**
   * Augmente la production par seconde
   * @param {Big} increase
   */
  public increasePerSecond (increase: Big) {
    this.setState({
      ...this.value,
      perSecond: this.value.perSecond.add(increase)
    });
  }

  public setAddModeratorProd (addModeratorProd: Big) {
    this.setState({
      ...this.value,
      addModeratorProd
    });
  }

}
