import { Injectable } from '@angular/core';
import { GenericStore } from './generic-store';
import Big from 'big.js';
import { StoreModule } from '../store.module';

/**
 * Compteurs de la partie
 */
export interface Counts {

  /**
   * Compteur actuel
   */
  currentCount: Big;

  /**
   * Compteur cumulé de toute la partie
   */
  totalCount: Big;

  /**
   * Nombre total de clics
   */
  totalClicks: number;

  /**
   * Nombre total de bans produits par les clics
   */
  totalCountClick: Big;
}

/**
 * Gère les compteurs de la partie
 */
@Injectable({
  providedIn: StoreModule
})
export class CountsStore extends GenericStore<Counts> {

  constructor () {
    // Etat initial
    super({
      currentCount: new Big(10000),
      totalCount: new Big(0),
      totalClicks: 0,
      totalCountClick: new Big(0)
    });
  }

  /**
   * Incrémente le compteur actuel (et a fortiori le compteur total) de la valeur passée en paramètre
   * @param {Big} increase
   */
  public increaseCount (increase: Big): void {
    this.setState({
      ...this.value,
      currentCount: this.value.currentCount.add(increase),
      totalCount: this.value.totalCount.add(increase)
    });
  }

  /**
   * Décrémente le compteur actuel
   * @param {Big} decrease
   */
  public decreaseCount (decrease: Big): void {
    this.setState({
      ...this.value,
      currentCount: this.value.currentCount.minus(decrease)
    });
  }

  /**
   * Permet d'augmenter de 1 le nombre de clics
   */
  public addClick (): void {
    this.setState({
      ...this.value,
      totalClicks: this.value.totalClicks + 1
    });
  }

  /**
   * Augmente le total à l'aide de clic
   * @param {Big} increase
   */
  public increaseClickCounts (increase: Big): void {
    this.setState({
      ...this.value,
      totalCountClick: this.value.totalCountClick.add(increase),
      currentCount: this.value.currentCount.add(increase),
      totalCount: this.value.totalCount.add(increase)
    });
  }
}
