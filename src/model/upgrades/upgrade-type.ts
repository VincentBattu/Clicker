export enum UpgradeType {
  IncreaseFlat,
  IncreaseScaling,
  DecreaseCost,
  DecreaseFlat,
  GameName
}

