import { UpgradeType } from './upgrade-type';
import Big from 'big.js';
import { UpgradeScope } from './upgrade-scope';
import { Currency } from './currency';
import { Language } from '../language';

export class Upgrade {

  /**
   * Identifiant de l'amélioration
   */
  private _id: number;

  /**
   * Type de l'amélioration
   */
  private _type: UpgradeType;

  /**
   * Portée de l'amélioration
   */
  private _scope: UpgradeScope;

  /**
   * Description de l'amélioration
   */
  private _description: Language;

  /**
   * Nom de l'amélioration
   */
  private _name: Language;

  /**
   * Augmentation produite par l'amélioration.
   * Ex : pour doubler la production : increase = 2
   */
  private _increase: Big;

  /**
   * Id du producteur concerné
   */
  private _producerId: number;

  /**
   * L'amélioration est-elle débloquée ?
   */
  private _unlocked = false;

  /**
   * Nombre d'éléments de type <code>unlockCurrency</code> pour le débloquer
   */
  private _unlockValue: number;

  /**
   * Durée de l'amélioration (en secondes)
   * Vaut 0 si la durée est illimitée.
   */
  private _duration = 0;

  /**
   * Prix de l'amélioration (devise : <code>priceCurrency</code>)
   */
  private _price: number;

  /**
   * Devise permettant de débloquer l'amélioration
   */
  private _unlockCurrency: Currency;

  /**
   * Dévise permettant d'acheter l'amélioration
   */
  private _priceCurrency: Currency;


  get type (): UpgradeType {
    return this._type;
  }

  set type (value: UpgradeType) {
    this._type = value;
  }

  get scope (): UpgradeScope {
    return this._scope;
  }

  set scope (value: UpgradeScope) {
    this._scope = value;
  }

  get description (): Language {
    return this._description;
  }

  set description (value: Language) {
    this._description = value;
  }

  get name (): Language {
    return this._name;
  }

  set name (value: Language) {
    this._name = value;
  }

  get increase (): Big {
    return this._increase;
  }

  set increase (value: Big) {
    this._increase = value;
  }

  get producerId (): number {
    return this._producerId;
  }

  set producerId (value: number) {
    this._producerId = value;
  }

  get unlocked (): boolean {
    return this._unlocked;
  }

  set unlocked (value: boolean) {
    this._unlocked = value;
  }

  get unlockValue (): number {
    return this._unlockValue;
  }

  set unlockValue (value: number) {
    this._unlockValue = value;
  }

  get duration (): number {
    return this._duration;
  }

  set duration (value: number) {
    this._duration = value;
  }

  get price (): number {
    return this._price;
  }

  set price (value: number) {
    this._price = value;
  }

  get unlockCurrency (): Currency {
    return this._unlockCurrency;
  }

  set unlockCurrency (value: Currency) {
    this._unlockCurrency = value;
  }

  get priceCurrency (): Currency {
    return this._priceCurrency;
  }

  set priceCurrency (value: Currency) {
    this._priceCurrency = value;
  }

  get id () {
    return this._id;
  }

  set id (value: number) {
    this._id = value;
  }
}

