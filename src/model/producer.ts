import Big from 'big.js';

export class Producer {

  /**
   * Identifiant du producteur
   */
  private _id: number;

  /**
   * Retourne le nombre de producteurs de ce type
   */
  private _number = 0;

  /**
   * Nombre actuel de bans par seconde
   */
  private _production: Big;

  /**
   * Réduction du price
   */
  private _decreaseCost = new Big(1);

  /**
   * Nom du producteur
   */
  private _name: string;

  /**
   * Retourne une description concernant le producteur
   */
  private _description: string;

  /**
   * Prix de base du producteur
   */
  private _basePrice: Big;

  /**
   * Level du producteur
   */
  private _level = 1;

  /**
   * Production additionnel
   * @type {Big}
   * @private
   */
  private _additionalProduction = new Big(0);


  get id (): number {
    return this._id;
  }

  set id (value: number) {
    this._id = value;
  }

  get number (): number {
    return this._number;
  }

  set number (value: number) {
    this._number = value;
  }

  get production (): Big {
    return this._production.add(this._additionalProduction);
  }

  set production (value: Big) {
    this._production = value;
  }

  get price (): Big {
    return this.basePrice.mul(new Big('1.15').pow(this.number)).mul(this._decreaseCost);
  }

  get decreaseCost (): Big {
    return this._decreaseCost;
  }

  set decreaseCost (value: Big) {
    this._decreaseCost = value;
  }

  get name (): string {
    return this._name;
  }

  set name (value: string) {
    this._name = value;
  }

  get description (): string {
    return this._description;
  }

  set description (value: string) {
    this._description = value;
  }

  get basePrice (): Big {
    return this._basePrice;
  }

  set basePrice (value: Big) {
    this._basePrice = value;
  }

  get level () {
   return this._level;
  }

  set level (value: number) {
    this._level = value;
  }

  get additionalProduction (): Big {
    return this._additionalProduction;
  }

  set additionalProduction (value: Big) {
    this._additionalProduction = value;
  }
}
