import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { AppComponent } from './components/app/app.component';
import { CountComponent } from './components/count/count.component';
import { ChatComponent } from './components/chat/chat.component';
import { BanButtonComponent } from './components/ban-button/ban-button.component';
import { ProducerComponent } from './components/producer/producer.component';
import { UpgradeComponent } from './components/upgrade/upgrade.component';
import { FormatBigNumberPipe } from './pipes/format-big-number.pipe';
import { MobxAngularModule } from 'mobx-angular';
import { FormatCurrencyPipe } from './pipes/format-currency.pipe';
import { ParametersComponent } from './components/parameters/parameters.component';
import { FormsModule } from '@angular/forms';
import { ProducersComponent } from './components/producers/producers.component';
import { UpgradesComponent } from './components/upgrades/upgrades.component';
import { StoreModule } from '../store/store.module';
import { PopupComponent } from './components/popup/popup.component';
import { HtmlSanitizerPipe } from './pipes/html-sanitizer.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslatePipe } from './pipes/translate.pipe';
import { registerLocaleData } from '@angular/common';

import localeFr from '@angular/common/locales/fr';
import { UpgradeTooltipContentComponent } from './components/upgrade-tooltip-content/upgrade-tooltip-content.component';
import { SuccessComponent } from './components/success/success.component';
import { PatchNoteComponent } from './components/patch-note/patch-note.component';

registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent,
    CountComponent,
    ChatComponent,
    BanButtonComponent,
    ProducersComponent,
    ProducerComponent,
    UpgradeComponent,
    FormatBigNumberPipe,
    FormatCurrencyPipe,
    ParametersComponent,
    ProducersComponent,
    UpgradesComponent,
    PopupComponent,
    HtmlSanitizerPipe,
    TranslatePipe,
    UpgradeTooltipContentComponent,
    SuccessComponent,
    PatchNoteComponent
  ],
  imports: [
    BrowserModule,
    MobxAngularModule,
    FormsModule,
    StoreModule,
    NgbModule.forRoot()
  ],
  providers: [
    FormatBigNumberPipe,
    { provide: LOCALE_ID, useValue: 'fr-FR' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
