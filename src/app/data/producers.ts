import { MODERATOR } from './producers/moderator';
import { BOT } from './producers/ban-bot';
import { JUDGE } from './producers/judge';
import { STREAMER } from './producers/streamer';
import { INQUISIDOR } from './producers/inquisidor';
import { PUNISHER } from './producers/punisher';
import { TWO_GODS } from './producers/two-gods';
import { GOD } from './producers/god';

export const PRODUCERS = [
  MODERATOR,
  BOT,
  JUDGE,
  STREAMER,
  INQUISIDOR,
  PUNISHER,
  GOD,
  TWO_GODS
];

