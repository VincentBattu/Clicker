import { MODERATOR_UPGRADES } from './upgrades/moderator-upgrades';
import { JUDGE_UPGRADES } from './upgrades/jugdge-upgrades';
import { STREAMER_UPGRADES } from './upgrades/streamer-upgrades';
import { GOD_UPGRADES } from './upgrades/god-upgrades';
import { GLOBAL_UPGRADES } from './upgrades/global-upgrades';
import { BOT_UPGRADES } from './upgrades/ban-bot-upgrades';

let upgrades = [];

upgrades = upgrades.concat(GLOBAL_UPGRADES);
upgrades = upgrades.concat(MODERATOR_UPGRADES);
upgrades = upgrades.concat(BOT_UPGRADES);
upgrades = upgrades.concat(JUDGE_UPGRADES);
upgrades = upgrades.concat(STREAMER_UPGRADES);
upgrades = upgrades.concat(GOD_UPGRADES);

export { upgrades as UPGRADES };
