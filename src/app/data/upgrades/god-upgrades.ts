import { Upgrade } from '../../../model/upgrades/upgrade';
import { UpgradeType } from '../../../model/upgrades/upgrade-type';
import { Currency } from '../../../model/upgrades/currency';
import { UpgradeScope } from '../../../model/upgrades/upgrade-scope';
import Big from 'big.js';
import { GOD } from '../producers/god';

const upgrades = [];

// ---------------------------------------------------------------------------------------------------------
//                                      AMELIORATION DU BPS
// ---------------------------------------------------------------------------------------------------------
const disciples = new Upgrade();
disciples.name = {
    fr: 'Disciples'
};
disciples.description = {
    fr: 'Ouais dieu toussa, prie-moi et ça sera bien'
};
disciples.price = 5_000_000;
disciples.unlockValue = 10;
disciples.increase = new Big('2');
disciples.type = UpgradeType.IncreaseFlat;
disciples.scope = UpgradeScope.Producer;
disciples.unlockCurrency = Currency.Producer;
disciples.priceCurrency = Currency.Ban;
disciples.producerId = GOD.id;
upgrades.push(disciples);

const monks = new Upgrade();
monks.name = {
    fr: 'Moines'
};
monks.description = {
    fr: 'Moines description'
};
monks.price = 10_000_000;
monks.unlockValue = 20;
monks.increase = new Big('2');
monks.type = UpgradeType.IncreaseFlat;
monks.scope = UpgradeScope.Producer;
monks.unlockCurrency = Currency.Producer;
monks.priceCurrency = Currency.Ban;
monks.producerId = GOD.id;
upgrades.push(monks);

const priests = new Upgrade();
priests.name = {
    fr: 'Prêtres'
};
priests.description = {
    fr: 'Prêtres description'
};
priests.price = 50_000_000;
priests.unlockValue = 35;
priests.increase = new Big('2');
priests.type = UpgradeType.IncreaseFlat;
priests.scope = UpgradeScope.Producer;
priests.unlockCurrency = Currency.Producer;
priests.priceCurrency = Currency.Ban;
priests.producerId = GOD.id;
upgrades.push(priests);

const apostles = new Upgrade();
apostles.name = {
    fr: 'Apôtres'
};
apostles.description = {
    fr: 'Apôtres description'
};
apostles.price = 50_000_000;
apostles.unlockValue = 50;
apostles.increase = new Big('2');
apostles.type = UpgradeType.IncreaseFlat;
apostles.scope = UpgradeScope.Producer;
apostles.unlockCurrency = Currency.Producer;
apostles.priceCurrency = Currency.Ban;
apostles.producerId = GOD.id;
upgrades.push(apostles);

const martyr = new Upgrade();
martyr.name = {
    fr: 'Martyr'
};
martyr.description = {
    fr: 'martyr toussa'
};
martyr.price = 5_000_000_000;
martyr.unlockValue = 75;
martyr.increase = new Big('2');
martyr.type = UpgradeType.IncreaseFlat;
martyr.scope = UpgradeScope.Producer;
martyr.unlockCurrency = Currency.Producer;
martyr.priceCurrency = Currency.Ban;
martyr.producerId = GOD.id;
upgrades.push(martyr);

const archeveque = new Upgrade();
archeveque.name = {
    fr: 'Archevêque'
};
archeveque.description = {
    fr: 'archeveque toussa'
};
archeveque.price = 5_000_000_000;
archeveque.unlockValue = 100;
archeveque.increase = new Big('3');
archeveque.type = UpgradeType.IncreaseFlat;
archeveque.scope = UpgradeScope.Producer;
archeveque.unlockCurrency = Currency.Producer;
archeveque.priceCurrency = Currency.Ban;
archeveque.producerId = GOD.id;
upgrades.push(archeveque);

const angel = new Upgrade();
angel.name = {
    fr: 'Ange'
};
angel.description = {
    fr: 'angel toussa'
};
angel.price = 50_000_000_000;
angel.unlockValue = 125;
angel.increase = new Big('2');
angel.type = UpgradeType.IncreaseFlat;
angel.scope = UpgradeScope.Producer;
angel.unlockCurrency = Currency.Producer;
angel.priceCurrency = Currency.Ban;
angel.producerId = GOD.id;
upgrades.push(angel);

const archangel = new Upgrade();
archangel.name = {
    fr: 'Archange'
};
archangel.description = {
    fr: 'archangel toussa'
};
archangel.price = 500_000_000_000;
archangel.unlockValue = 150;
archangel.increase = new Big('2');
archangel.type = UpgradeType.IncreaseFlat;
archangel.scope = UpgradeScope.Producer;
archangel.unlockCurrency = Currency.Producer;
archangel.priceCurrency = Currency.Ban;
archangel.producerId = GOD.id;
upgrades.push(archangel);

// ---------------------------------------------------------------------------------------------------------
//                                      Réduction du coût
// ---------------------------------------------------------------------------------------------------------

const meditation = new Upgrade();
meditation.name = {
    fr: 'Méditation'
};
meditation.description = {
    fr: 'Méditation description'
};
meditation.price = 7_500_000;
meditation.unlockValue = 10;
meditation.increase = new Big('0.94');
meditation.type = UpgradeType.DecreaseCost;
meditation.scope = UpgradeScope.Producer;
meditation.unlockCurrency = Currency.Producer;
meditation.priceCurrency = Currency.Ban;
meditation.producerId = GOD.id;
upgrades.push(meditation);

const pray = new Upgrade();
pray.name = {
    fr: 'Prière'
};
pray.description = {
    fr: 'Prières description'
};
pray.price = 15_000_000;
pray.unlockValue = 20;
pray.increase = new Big('0.88');
pray.type = UpgradeType.DecreaseCost;
pray.scope = UpgradeScope.Producer;
pray.unlockCurrency = Currency.Producer;
pray.priceCurrency = Currency.Ban;
pray.producerId = GOD.id;
upgrades.push(pray);

const fast = new Upgrade();
fast.name = {
    fr: 'Jeûne'
};
fast.description = {
    fr: 'Jeûne description'
};
fast.price = 75_000_000;
fast.unlockValue = 35;
fast.increase = new Big('0.82');
fast.type = UpgradeType.DecreaseCost;
fast.scope = UpgradeScope.Producer;
fast.unlockCurrency = Currency.Producer;
fast.priceCurrency = Currency.Ban;
fast.producerId = GOD.id;
upgrades.push(fast);

const molestation = new Upgrade();
molestation.name = {
  fr: 'Flagellation'
};
molestation.description = {
  fr: 'Desc'
};
molestation.price = 100_000_000_000;
molestation.unlockValue = 50;
molestation.increase = new Big('0.76');
molestation.type = UpgradeType.DecreaseCost;
molestation.scope = UpgradeScope.Producer;
molestation.unlockCurrency = Currency.Producer;
molestation.priceCurrency = Currency.Ban;
molestation.producerId = GOD.id;
upgrades.push(molestation);

const exodus = new Upgrade();
exodus.name = {
  fr: 'Exode'
};
exodus.description = {
  fr: 'Desc'
};
exodus.price = 1_000_000_000_000;
exodus.unlockValue = 75;
exodus.increase = new Big('0.70');
exodus.type = UpgradeType.DecreaseCost;
exodus.scope = UpgradeScope.Producer;
exodus.unlockCurrency = Currency.Producer;
exodus.priceCurrency = Currency.Ban;
exodus.producerId = GOD.id;
upgrades.push(exodus);

const confession = new Upgrade();
confession.name = {
  fr: 'Confession'
};
confession.description = {
  fr: 'Desc'
};
confession.price = 10_000_000_000_000;
confession.unlockValue = 100;
confession.increase = new Big('0.64');
confession.type = UpgradeType.DecreaseCost;
confession.scope = UpgradeScope.Producer;
confession.unlockCurrency = Currency.Producer;
confession.priceCurrency = Currency.Ban;
confession.producerId = GOD.id;
upgrades.push(confession);

const lapidation = new Upgrade();
lapidation.name = {
  fr: 'Lapidation'
};
lapidation.description = {
  fr: 'Desc'
};
lapidation.price = 100_000_000_000_000;
lapidation.unlockValue = 125;
lapidation.increase = new Big('0.58');
lapidation.type = UpgradeType.DecreaseCost;
lapidation.scope = UpgradeScope.Producer;
lapidation.unlockCurrency = Currency.Producer;
lapidation.priceCurrency = Currency.Ban;
lapidation.producerId = GOD.id;
upgrades.push(lapidation);

const crusade = new Upgrade();
crusade.name = {
  fr: 'Guerre sainte'
};
crusade.description = {
  fr: 'Desc'
};
crusade.price = 1_000_000_000_000_000;
crusade.unlockValue = 150;
crusade.increase = new Big('0.52');
crusade.type = UpgradeType.DecreaseCost;
crusade.scope = UpgradeScope.Producer;
crusade.unlockCurrency = Currency.Producer;
crusade.priceCurrency = Currency.Ban;
crusade.producerId = GOD.id;
upgrades.push(crusade);


export { upgrades as GOD_UPGRADES };
