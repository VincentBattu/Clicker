import { UpgradeType } from '../../../model/upgrades/upgrade-type';
import { Currency } from '../../../model/upgrades/currency';
import { MODERATOR } from '../producers/moderator';
import { Upgrade } from '../../../model/upgrades/upgrade';
import { UpgradeScope } from '../../../model/upgrades/upgrade-scope';
import Big from 'big.js';

const upgrades = [];
const producerId = MODERATOR.id;

// ---------------------------------------------------------------------------------------------------------
//                                      AMELIORATION DU BPS
// ---------------------------------------------------------------------------------------------------------
const tea = new Upgrade();
tea.name = {
  fr: 'Thé'
};
tea.description = {
  fr: `Un peu de thé ne pourra pas faire de mal pour maintenir ce gentil modérateur
  éveillé`
};
tea.price = 100;
tea.unlockValue = 10;
tea.increase = new Big('2');
tea.type = UpgradeType.IncreaseFlat;
tea.scope = UpgradeScope.Producer;
tea.unlockCurrency = Currency.Producer;
tea.priceCurrency = Currency.Ban;
tea.producerId = MODERATOR.id;
upgrades.push(tea);


const coffee = new Upgrade();
coffee.name = {
  fr: 'Café'
};
coffee.description = {
  fr: 'Bon c\'était peut-être un peu léger le thé'
};
coffee.price = 500;
coffee.unlockValue = 20;
coffee.increase = new Big('2');
coffee.type = UpgradeType.IncreaseFlat;
coffee.scope = UpgradeScope.Producer;
coffee.unlockCurrency = Currency.Producer;
coffee.priceCurrency = Currency.Ban;
coffee.producerId = MODERATOR.id;
upgrades.push(coffee);

const redbull = new Upgrade();
redbull.name = {
  fr: 'Redbull'
};
redbull.description = {
  fr: `Quand la cafeine ne suffit plus (et en plus on peut voler)`
};
redbull.price = 5_000;
redbull.unlockValue = 35;
redbull.increase = new Big('2');
redbull.type = UpgradeType.IncreaseFlat;
redbull.scope = UpgradeScope.Producer;
redbull.unlockCurrency = Currency.Producer;
redbull.priceCurrency = Currency.Ban;
redbull.producerId = MODERATOR.id;
upgrades.push(redbull);

const grass = new Upgrade();
grass.name = {
  fr: 'Herbe'
};
grass.description = {
  fr: 'Spécial vegans'
};
grass.price = 50_000;
grass.unlockValue = 50;
grass.increase = new Big('0.1');
grass.type = UpgradeType.IncreaseScaling;
grass.scope = UpgradeScope.Producer;
grass.unlockCurrency = Currency.Producer;
grass.priceCurrency = Currency.Ban;
grass.producerId = MODERATOR.id;
upgrades.push(grass);

const cocaine = new Upgrade();
cocaine.name = {
  fr: 'Cocaine'
};
cocaine.description = {
  fr: `Vous avez votre propre plantation ! Ca vous évitera d'acheter des trucs venus d'on sait pas trop quel pays`
};
cocaine.price = 1_000_000;
cocaine.unlockValue = 75;
cocaine.increase = new Big('0.5');
cocaine.type = UpgradeType.IncreaseScaling;
cocaine.scope = UpgradeScope.Producer;
cocaine.unlockCurrency = Currency.Producer;
cocaine.priceCurrency = Currency.Ban;
cocaine.producerId = MODERATOR.id;
upgrades.push(cocaine);

const epo = new Upgrade();
epo.name = {
  fr: 'EPO'
};
epo.description = {
  fr: 'Si jamais, vous pourrez vous reconvertir dans le cyclisme'
};
epo.price = 100_000_000;
epo.unlockValue = 100;
epo.increase = new Big('1');
epo.type = UpgradeType.IncreaseScaling;
epo.scope = UpgradeScope.Producer;
epo.unlockCurrency = Currency.Producer;
epo.priceCurrency = Currency.Ban;
epo.producerId = MODERATOR.id;
upgrades.push(epo);

const msub = new Upgrade();
msub.name = {
  fr: 'Substance M'
};
msub.description = {
  fr: 'Pour les amateurs'
};
msub.price = 2_000_000_000;
msub.unlockValue = 125;
msub.increase = new Big('2');
msub.type = UpgradeType.IncreaseScaling;
msub.scope = UpgradeScope.Producer;
msub.unlockCurrency = Currency.Producer;
msub.priceCurrency = Currency.Ban;
msub.producerId = MODERATOR.id;
upgrades.push(msub);

const kryptonite = new Upgrade();
kryptonite.name = {
  fr: 'Kryptonite'
};
kryptonite.description = {
  fr: 'Des fois, il faut aller loin pour être un minimum efficace'
};
kryptonite.price = 100_000_000_000;
kryptonite.unlockValue = 150;
kryptonite.increase = new Big('5');
kryptonite.type = UpgradeType.IncreaseScaling;
kryptonite.scope = UpgradeScope.Producer;
kryptonite.unlockCurrency = Currency.Producer;
kryptonite.priceCurrency = Currency.Ban;
kryptonite.producerId = MODERATOR.id;
upgrades.push(kryptonite);


// ---------------------------------------------------------------------------------------------------------
//                                      Réduction du coût
// ---------------------------------------------------------------------------------------------------------
const casual = new Upgrade();
casual.name = {
  fr: 'Casuel'
};
casual.description = {
  fr: 'Casuel description'
};
casual.price = 200;
casual.unlockValue = 10;
casual.increase = new Big('0.94');
casual.type = UpgradeType.DecreaseCost;
casual.scope = UpgradeScope.Producer;
casual.unlockCurrency = Currency.Producer;
casual.priceCurrency = Currency.Ban;
casual.producerId = MODERATOR.id;
upgrades.push(casual);

const engaged = new Upgrade();
engaged.name = {
  fr: 'Engagé'
};
engaged.description = {
  fr: 'Engagé description'
};
engaged.price = 700;
engaged.unlockValue = 20;
engaged.increase = new Big('0.88');
engaged.type = UpgradeType.DecreaseCost;
engaged.scope = UpgradeScope.Producer;
engaged.unlockCurrency = Currency.Producer;
engaged.priceCurrency = Currency.Ban;
engaged.producerId = MODERATOR.id;
upgrades.push(engaged);

const hobby = new Upgrade();
hobby.name = {
  fr: 'Hobby'
};
hobby.description = {
  fr: 'Hobby description'
};
hobby.price = 7_000;
hobby.unlockValue = 35;
hobby.increase = new Big('0.82');
hobby.type = UpgradeType.DecreaseCost;
hobby.scope = UpgradeScope.Producer;
hobby.unlockCurrency = Currency.Producer;
hobby.priceCurrency = Currency.Ban;
hobby.producerId = MODERATOR.id;
upgrades.push(hobby);

const volunteer = new Upgrade();
volunteer.name = {
  fr: 'Bénévole motivé'
};
volunteer.description = {
  fr: 'Bénévole motivé description'
};
volunteer.price = 60_000;
volunteer.unlockValue = 50;
volunteer.increase = new Big('0.76');
volunteer.type = UpgradeType.DecreaseCost;
volunteer.scope = UpgradeScope.Producer;
volunteer.unlockCurrency = Currency.Producer;
volunteer.priceCurrency = Currency.Ban;
volunteer.producerId = MODERATOR.id;
upgrades.push(volunteer);

const fan = new Upgrade();
fan.name = {
  fr: 'Fan'
};
fan.description = {
  fr: 'Desc'
};
fan.price = 1_200_000;
fan.unlockValue = 75;
fan.increase = new Big('0.70');
fan.type = UpgradeType.DecreaseCost;
fan.scope = UpgradeScope.Producer;
fan.unlockCurrency = Currency.Producer;
fan.priceCurrency = Currency.Ban;
fan.producerId = MODERATOR.id;
upgrades.push(fan);

const banstudent = new Upgrade();
banstudent.name = {
  fr: 'Apprenti bannisseur'
};
banstudent.description = {
  fr: 'Desc'
};
banstudent.price = 12_000_000;
banstudent.unlockValue = 100;
banstudent.increase = new Big('0.64');
banstudent.type = UpgradeType.DecreaseCost;
banstudent.scope = UpgradeScope.Producer;
banstudent.unlockCurrency = Currency.Producer;
banstudent.priceCurrency = Currency.Ban;
banstudent.producerId = MODERATOR.id;
upgrades.push(banstudent);

const confirmedbanner = new Upgrade();
confirmedbanner.name = {
  fr: 'Bannnisseur zélé'
};
confirmedbanner.description = {
  fr: 'Desc'
};
confirmedbanner.price = 2_200_000_000;
confirmedbanner.unlockValue = 125;
confirmedbanner.increase = new Big('0.58');
confirmedbanner.type = UpgradeType.DecreaseCost;
confirmedbanner.scope = UpgradeScope.Producer;
confirmedbanner.unlockCurrency = Currency.Producer;
confirmedbanner.priceCurrency = Currency.Ban;
confirmedbanner.producerId = MODERATOR.id;
upgrades.push(confirmedbanner);

const repurgator = new Upgrade();
repurgator.name = {
  fr: 'Répurgateur'
};
repurgator.description = {
  fr: 'Desc'
};
repurgator.price = 100_000_000_000;
repurgator.unlockValue = 150;
repurgator.increase = new Big('0.50');
repurgator.type = UpgradeType.DecreaseCost;
repurgator.scope = UpgradeScope.Producer;
repurgator.unlockCurrency = Currency.Producer;
repurgator.priceCurrency = Currency.Ban;
repurgator.producerId = MODERATOR.id;
upgrades.push(repurgator);

export { upgrades as MODERATOR_UPGRADES };
