import { Currency } from '../../../model/upgrades/currency';
import Big from 'big.js';
import { UpgradeType } from '../../../model/upgrades/upgrade-type';
import { UpgradeScope } from '../../../model/upgrades/upgrade-scope';
import { Upgrade } from '../../../model/upgrades/upgrade';
import { BOT } from '../producers/ban-bot';

const upgrades = [];

// ---------------------------------------------------------------------------------------------------------
//                                      AMELIORATION DU BPS
// ---------------------------------------------------------------------------------------------------------
const adsl = new Upgrade();
adsl.name = {
  fr: 'ADSL'
};
adsl.description = {
  fr: 'N\'est-ce pas mieux que votre vieux modem 56k ?'
};
adsl.price = 3_200;
adsl.unlockValue = 10;
adsl.increase = new Big('2');
adsl.type = UpgradeType.IncreaseFlat;
adsl.scope = UpgradeScope.Producer;
adsl.unlockCurrency = Currency.Producer;
adsl.priceCurrency = Currency.Ban;
adsl.producerId = BOT.id;
upgrades.push(adsl);

const vdsl = new Upgrade();
vdsl.name = {
  fr: 'VDSL'
};
vdsl.description = {
  fr: `Bon la campagne c'est bien marrant 5 minutes mais il s'agirait de penser à l'efficacité`
};
vdsl.price = 12_000;
vdsl.unlockValue = 20;
vdsl.increase = new Big(2);
vdsl.type = UpgradeType.IncreaseFlat;
vdsl.scope = UpgradeScope.Producer;
vdsl.unlockCurrency = Currency.Producer;
vdsl.priceCurrency = Currency.Ban;
vdsl.producerId = BOT.id;
upgrades.push(vdsl);

const fiber = new Upgrade();
fiber.name = {
  fr: 'Fibre optique'
};
fiber.description = {
  fr: 'ELLE ARRIVE !!!!!'
};
fiber.price = 120_000;
fiber.unlockValue = 35;
fiber.increase = new Big(2);
fiber.type = UpgradeType.IncreaseFlat;
fiber.scope = UpgradeScope.Producer;
fiber.unlockCurrency = Currency.Producer;
fiber.priceCurrency = Currency.Ban;
fiber.producerId = BOT.id;
upgrades.push(fiber);

const google = new Upgrade();
google.name = {
  fr: 'Google'
};
google.description = {
  fr: 'Google description'
};
google.price = 1_000_000;
google.unlockValue = 50;
google.increase = new Big(2);
google.type = UpgradeType.IncreaseFlat;
google.scope = UpgradeScope.Producer;
google.unlockCurrency = Currency.Producer;
google.priceCurrency = Currency.Ban;
google.producerId = BOT.id;
upgrades.push(google);

const nasa = new Upgrade();
nasa.name = {
  fr: 'NASA'
};
nasa.description = {
  fr: 'Nasa description'
};
nasa.price = 25_000_000;
nasa.unlockValue = 75;
nasa.increase = new Big(2);
nasa.type = UpgradeType.IncreaseFlat;
nasa.scope = UpgradeScope.Producer;
nasa.unlockCurrency = Currency.Producer;
nasa.priceCurrency = Currency.Ban;
nasa.producerId = BOT.id;
upgrades.push(nasa);

const hyperspace = new Upgrade();
hyperspace.name = {
  fr: 'Hyperespace'
};
hyperspace.description = {
  fr: `Pour ban vers l'infini et au delà`
};
hyperspace.price = 1_000_000_000;
hyperspace.unlockValue = 100;
hyperspace.increase = new Big(3);
hyperspace.type = UpgradeType.IncreaseFlat;
hyperspace.scope = UpgradeScope.Producer;
hyperspace.unlockCurrency = Currency.Producer;
hyperspace.priceCurrency = Currency.Ban;
hyperspace.producerId = BOT.id;
upgrades.push(hyperspace);

const lightspeed = new Upgrade();
lightspeed.name = {
  fr: 'Vitesse lumière'
};
lightspeed.description = {
  fr: 'Vitesse lumière description'
};
lightspeed.price = 30_000_000_000;
lightspeed.unlockValue = 125;
lightspeed.increase = new Big(2);
lightspeed.type = UpgradeType.IncreaseFlat;
lightspeed.scope = UpgradeScope.Producer;
lightspeed.unlockCurrency = Currency.Producer;
lightspeed.priceCurrency = Currency.Ban;
lightspeed.producerId = BOT.id;
upgrades.push(lightspeed);

const advl = new Upgrade();
advl.name = {
  fr: 'ADVL'
};
advl.description = {
  fr: 'ADVL description'
};
advl.price = 1_000_000_000_000;
advl.unlockValue = 150;
advl.increase = new Big(2);
advl.type = UpgradeType.IncreaseFlat;
advl.scope = UpgradeScope.Producer;
advl.unlockCurrency = Currency.Producer;
advl.priceCurrency = Currency.Ban;
advl.producerId = BOT.id;
upgrades.push(advl);

// ---------------------------------------------------------------------------------------------------------
//                                      Réduction du coût
// ---------------------------------------------------------------------------------------------------------
const punchedCard = new Upgrade();
punchedCard.name = {
  fr: 'Carte perforée'
};
punchedCard.description = {
  fr: `Ha la bonne carte perforée, fallait bien commencer quelque part aussi, mais va falloir se mettre à jour`
};
punchedCard.price = 3_000;
punchedCard.unlockValue = 10;
punchedCard.increase = new Big('0.94');
punchedCard.type = UpgradeType.DecreaseCost;
punchedCard.scope = UpgradeScope.Producer;
punchedCard.unlockCurrency = Currency.Producer;
punchedCard.priceCurrency = Currency.Ban;
punchedCard.producerId = BOT.id;
upgrades.push(punchedCard);

const python = new Upgrade();
python.name = {
  fr: 'Python'
};
python.description = {
  fr: `On est tous passé par là, c'est pas si grave vous savez. Puis ça va quand même plus vite que les cartes perforées`
};
python.price = 15_000;
python.unlockValue = 20;
python.increase = new Big('0.88');
python.type = UpgradeType.DecreaseCost;
python.scope = UpgradeScope.Producer;
python.unlockCurrency = Currency.Producer;
python.priceCurrency = Currency.Ban;
python.producerId = BOT.id;
upgrades.push(python);

const java = new Upgrade();
java.name = {
  fr: 'Java'
};
java.description = {
  fr: `Ca y est, on a enfn trouvé de vrais développeurs pour écrire ce satané bot dans un vrai langage. Résultats au rendez-vous !`
};
java.price = 125_000;
java.unlockValue = 35;
java.increase = new Big('0.82');
java.type = UpgradeType.DecreaseCost;
java.scope = UpgradeScope.Producer;
java.unlockCurrency = Currency.Producer;
java.priceCurrency = Currency.Ban;
java.producerId = BOT.id;
upgrades.push(java);

const cPlusPlus = new Upgrade();
cPlusPlus.name = {
  fr: 'C++'
};
cPlusPlus.description = {
  fr: `Malgré sa syntaxe bien inférieure à son prédécesseur, il faut bien admettre que l'on y gagne quand même en performance.
     Attention, on est quand même sur quelque chose de très ténu !`
};
cPlusPlus.price = 5_000_000;
cPlusPlus.unlockValue = 50;
cPlusPlus.increase = new Big('0.76');
cPlusPlus.type = UpgradeType.DecreaseCost;
cPlusPlus.scope = UpgradeScope.Producer;
cPlusPlus.unlockCurrency = Currency.Producer;
cPlusPlus.priceCurrency = Currency.Ban;
cPlusPlus.producerId = BOT.id;
upgrades.push(cPlusPlus);

const assembler = new Upgrade();
assembler.name = {
  fr: 'Assembler'
};
assembler.description = {
  fr: `Le top de l'optimisation ! On sait plus trop ce qu'il fait mais ça a l'air d'être efficace, très efficace même`
};
assembler.price = 22_000_000;
assembler.unlockValue = 75;
assembler.increase = new Big('0.70');
assembler.type = UpgradeType.DecreaseCost;
assembler.scope = UpgradeScope.Producer;
assembler.unlockCurrency = Currency.Producer;
assembler.priceCurrency = Currency.Ban;
assembler.producerId = BOT.id;
upgrades.push(assembler);

const binary = new Upgrade();
binary.name = {
  fr: 'Binaire'
};
binary.description = {
  fr: `Binaire description`
};
binary.price = 900_000_000;
binary.unlockValue = 100;
binary.increase = new Big('0.64');
binary.type = UpgradeType.DecreaseCost;
binary.scope = UpgradeScope.Producer;
binary.unlockCurrency = Currency.Producer;
binary.priceCurrency = Currency.Ban;
binary.producerId = BOT.id;
upgrades.push(binary);

const quanticComputer = new Upgrade();
quanticComputer.name = {
  fr: 'Ordinateur quantique'
};
quanticComputer.description = {
  fr: 'quanticComputer toussa'
};
quanticComputer.price = 32_000_000_000;
quanticComputer.unlockValue = 125;
quanticComputer.increase = new Big('0.58');
quanticComputer.type = UpgradeType.DecreaseCost;
quanticComputer.scope = UpgradeScope.Producer;
quanticComputer.unlockCurrency = Currency.Producer;
quanticComputer.priceCurrency = Currency.Ban;
quanticComputer.producerId = BOT.id;
upgrades.push(quanticComputer);

const summit = new Upgrade();
summit.name = {
  fr: 'Summit'
};
summit.description = {
  fr: 'summit toussa'
};
summit.price = 900_000_000_000;
summit.unlockValue = 150;
summit.increase = new Big('0.52');
summit.type = UpgradeType.DecreaseCost;
summit.scope = UpgradeScope.Producer;
summit.unlockCurrency = Currency.Producer;
summit.priceCurrency = Currency.Ban;
summit.producerId = BOT.id;
upgrades.push(summit);

export { upgrades as BOT_UPGRADES };
