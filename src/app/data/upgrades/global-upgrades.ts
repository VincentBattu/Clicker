import { Currency } from '../../../model/upgrades/currency';
import { UpgradeType } from '../../../model/upgrades/upgrade-type';
import { UpgradeScope } from '../../../model/upgrades/upgrade-scope';
import { Upgrade } from '../../../model/upgrades/upgrade';
import Big from 'big.js';

const upgrades = [];

const gameName = new Upgrade();
gameName.name = {
  fr: 'Nom de la partie'
};
gameName.description = {
  fr: 'Pouvoir changer le nom de la partie, ça peut être sympa non ?'
};
gameName.price = 100;
gameName.unlockValue = 1;
gameName.type = UpgradeType.GameName;
gameName.scope = UpgradeScope.Global;
gameName.unlockCurrency = Currency.Ban;
gameName.priceCurrency = Currency.Ban;
upgrades.push(gameName);

// ---------------------------------------------------------------------------------------------------------
//                                      Amélioration du clic
// ---------------------------------------------------------------------------------------------------------

const officeMouse = new Upgrade();
officeMouse.name = {
  fr: 'Souris bureautique'
};
officeMouse.description = {
  fr: 'Ca vaut pas grand chose, mais ça reste mieux que votre vieil ami le pad'
};
officeMouse.price = 100;
officeMouse.unlockValue = 100;
officeMouse.increase = new Big(2);
officeMouse.type = UpgradeType.IncreaseFlat;
officeMouse.scope = UpgradeScope.Click;
officeMouse.unlockCurrency = Currency.Ban;
officeMouse.priceCurrency = Currency.Ban;
upgrades.push(officeMouse);

const intelliMouse = new Upgrade();
intelliMouse.name = {
  fr: 'IntelliMouse'
};
intelliMouse.description = {
  fr: 'On en trouve encore des comme ça ?'
};
intelliMouse.price = 1000;
intelliMouse.unlockValue = 1000;
intelliMouse.increase = new Big(2);
intelliMouse.type = UpgradeType.IncreaseFlat;
intelliMouse.scope = UpgradeScope.Click;
intelliMouse.unlockCurrency = Currency.Ban;
intelliMouse.priceCurrency = Currency.Ban;
upgrades.push(intelliMouse);

const spiritOfMouse = new Upgrade();
spiritOfMouse.name = {
  fr: 'Spirit of Mouse'
};
spiritOfMouse.description = {
  fr: 'On en trouve encore des comme ça ?'
};
spiritOfMouse.price = 10_000;
spiritOfMouse.unlockValue = 10_000;
spiritOfMouse.increase = new Big(2);
spiritOfMouse.type = UpgradeType.IncreaseFlat;
spiritOfMouse.scope = UpgradeScope.Click;
spiritOfMouse.unlockCurrency = Currency.Ban;
spiritOfMouse.priceCurrency = Currency.Ban;
upgrades.push(spiritOfMouse);

const mouZer = new Upgrade();
mouZer.name = {
  fr: 'MouZer'
};
mouZer.description = {
  fr: 'On en trouve encore des comme ça ?'
};
mouZer.price = 100_000;
mouZer.unlockValue = 100_000;
mouZer.increase = new Big(2);
mouZer.type = UpgradeType.IncreaseFlat;
mouZer.scope = UpgradeScope.Click;
mouZer.unlockCurrency = Currency.Ban;
mouZer.priceCurrency = Currency.Ban;
upgrades.push(mouZer);

const logiMouse = new Upgrade();
logiMouse.name = {
  fr: 'LogiMouse'
};
logiMouse.description = {
  fr: 'On en trouve encore des comme ça ?'
};
logiMouse.price = 1_000_000;
logiMouse.unlockValue = 1_000_000;
logiMouse.increase = new Big(2);
logiMouse.type = UpgradeType.IncreaseFlat;
logiMouse.scope = UpgradeScope.Click;
logiMouse.unlockCurrency = Currency.Ban;
logiMouse.priceCurrency = Currency.Ban;
upgrades.push(logiMouse);

// const save = new Upgrade();
// save.name = 'Sauvegarde de la partie';
// save.description = 'Pouvoir sauvegarder la partie pour ne pas tout recommencer à chaque fois';
// save.price = 10;
// save.unlockValue = 0;
// save.type = UpgradeType.GameName;
// save.scope = UpgradeScope.Global;
// save.unlockCurrency = Currency.Ban;
// save.priceCurrency = Currency.Ban;
// upgrades.push(save);

export { upgrades as GLOBAL_UPGRADES };
