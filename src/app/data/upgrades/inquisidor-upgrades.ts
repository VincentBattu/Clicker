import { Upgrade } from '../../../model/upgrades/upgrade';
import { UpgradeType } from '../../../model/upgrades/upgrade-type';
import { UpgradeScope } from '../../../model/upgrades/upgrade-scope';
import { Currency } from '../../../model/upgrades/currency';
import Big from 'big.js';
import { INQUISIDOR } from '../producers/inquisidor';

const upgrades = [];

// ---------------------------------------------------------------------------------------------------------
//                                      AMELIORATION DU BPS
// ---------------------------------------------------------------------------------------------------------
const mallet = new Upgrade();
mallet.name = {
    fr: 'Maillet'
};
mallet.description = {
    fr: `L'ustensile indispensable`
};
mallet.price = 800_000;
mallet.unlockValue = 10;
mallet.increase = new Big('2');
mallet.type = UpgradeType.IncreaseFlat;
mallet.scope = UpgradeScope.Producer;
mallet.unlockCurrency = Currency.Producer;
mallet.priceCurrency = Currency.Ban;
mallet.producerId = INQUISIDOR.id;
upgrades.push(mallet);

const wig = new Upgrade();
wig.name = {
    fr: 'Perruque'
};
wig.description = {
    fr: 'A la mode depuis Louis XIV'
};
wig.price = 8_000_000;
wig.unlockValue = 20;
wig.increase = new Big('2');
wig.type = UpgradeType.IncreaseFlat;
wig.scope = UpgradeScope.Producer;
wig.unlockCurrency = Currency.Producer;
wig.priceCurrency = Currency.Ban;
wig.producerId = INQUISIDOR.id;
upgrades.push(wig);

const hammer = new Upgrade();
hammer.name = {
    fr: 'Marteau'
};
hammer.description = {
    fr: `Silence s'il vous plaît ! La on parle de respect`
};
hammer.price = 80_000_000;
hammer.unlockValue = 35;
hammer.increase = new Big('2');
hammer.type = UpgradeType.IncreaseFlat;
hammer.scope = UpgradeScope.Producer;
hammer.unlockCurrency = Currency.Producer;
hammer.priceCurrency = Currency.Ban;
hammer.producerId = INQUISIDOR.id;
upgrades.push(hammer);

const dress = new Upgrade();
dress.name = {
  fr: 'Robe'
};
dress.description = {
  fr: 'Desc'
};
dress.price = 800_000_000;
dress.unlockValue = 50;
dress.increase = new Big('2');
dress.type = UpgradeType.IncreaseFlat;
dress.scope = UpgradeScope.Producer;
dress.unlockCurrency = Currency.Producer;
dress.priceCurrency = Currency.Ban;
dress.producerId = INQUISIDOR.id;
upgrades.push(dress);

const diploma = new Upgrade();
diploma.name = {
  fr: 'Diplome'
};
diploma.description = {
  fr: 'Desc'
};
diploma.price = 8_000_000_000;
diploma.unlockValue = 75;
diploma.increase = new Big('2');
diploma.type = UpgradeType.IncreaseFlat;
diploma.scope = UpgradeScope.Producer;
diploma.unlockCurrency = Currency.Producer;
diploma.priceCurrency = Currency.Ban;
diploma.producerId = INQUISIDOR.id;
upgrades.push(diploma);

const judgeDread = new Upgrade();
judgeDread.name = {
  fr: 'Judge Dread'
};
judgeDread.description = {
  fr: 'Desc'
};
judgeDread.price = 80_000_000_000;
judgeDread.unlockValue = 100;
judgeDread.increase = new Big('3');
judgeDread.type = UpgradeType.IncreaseFlat;
judgeDread.scope = UpgradeScope.Producer;
judgeDread.unlockCurrency = Currency.Producer;
judgeDread.priceCurrency = Currency.Ban;
judgeDread.producerId = INQUISIDOR.id;
upgrades.push(judgeDread);

const thanos = new Upgrade();
thanos.name = {
  fr: 'Thanos'
};
thanos.description = {
  fr: 'Desc'
};
thanos.price = 800_000_000_000;
thanos.unlockValue = 125;
thanos.increase = new Big('2');
thanos.type = UpgradeType.IncreaseFlat;
thanos.scope = UpgradeScope.Producer;
thanos.unlockCurrency = Currency.Producer;
thanos.priceCurrency = Currency.Ban;
thanos.producerId = INQUISIDOR.id;
upgrades.push(thanos);

const judgeDead = new Upgrade();
judgeDead.name = {
  fr: 'Judge Dead'
};
judgeDead.description = {
  fr: 'Desc'
};
judgeDead.price = 80_000_000_000_000;
judgeDead.unlockValue = 150;
judgeDead.increase = new Big('2');
judgeDead.type = UpgradeType.IncreaseFlat;
judgeDead.scope = UpgradeScope.Producer;
judgeDead.unlockCurrency = Currency.Producer;
judgeDead.priceCurrency = Currency.Ban;
judgeDead.producerId = INQUISIDOR.id;
upgrades.push(judgeDead);


// ---------------------------------------------------------------------------------------------------------
//                                      Réduction du coût
// ---------------------------------------------------------------------------------------------------------

const localLawBook = new Upgrade();
localLawBook.name = {
    fr: 'Livre de lois locales'
};
localLawBook.description = {
    fr: 'Livre de lois locales description'
};
localLawBook.price = 75_000;
localLawBook.unlockValue = 10;
localLawBook.increase = new Big('0.94');
localLawBook.type = UpgradeType.DecreaseCost;
localLawBook.scope = UpgradeScope.Producer;
localLawBook.unlockCurrency = Currency.Producer;
localLawBook.priceCurrency = Currency.Ban;
localLawBook.producerId = INQUISIDOR.id;
upgrades.push(localLawBook);

const regionalLawBook = new Upgrade();
regionalLawBook.name = {
    fr: 'Livre de lois régionales'
};
regionalLawBook.description = {
    fr: 'Livre de lois régionales description'
};
regionalLawBook.price = 10_000_000;
regionalLawBook.unlockValue = 20;
regionalLawBook.increase = new Big('0.88');
regionalLawBook.type = UpgradeType.DecreaseCost;
regionalLawBook.scope = UpgradeScope.Producer;
regionalLawBook.unlockCurrency = Currency.Producer;
regionalLawBook.priceCurrency = Currency.Ban;
regionalLawBook.producerId = INQUISIDOR.id;
upgrades.push(regionalLawBook);

const nationalLawBook = new Upgrade();
nationalLawBook.name = {
    fr: 'Livre de lois nationales'
};
nationalLawBook.description = {
    fr: 'Livre de lois nationales description'
};
nationalLawBook.price = 100_000_000;
nationalLawBook.unlockValue = 35;
nationalLawBook.increase = new Big('0.82');
nationalLawBook.type = UpgradeType.DecreaseCost;
nationalLawBook.scope = UpgradeScope.Producer;
nationalLawBook.unlockCurrency = Currency.Producer;
nationalLawBook.priceCurrency = Currency.Ban;
nationalLawBook.producerId = INQUISIDOR.id;
upgrades.push(nationalLawBook);

const internationalLawBook = new Upgrade();
internationalLawBook.name = {
  fr: 'Livre de lois internationales'
};
internationalLawBook.description = {
  fr: 'Desc'
};
internationalLawBook.price = 10_000_000_000;
internationalLawBook.unlockValue = 50;
internationalLawBook.increase = new Big('0.76');
internationalLawBook.type = UpgradeType.DecreaseCost;
internationalLawBook.scope = UpgradeScope.Producer;
internationalLawBook.unlockCurrency = Currency.Producer;
internationalLawBook.priceCurrency = Currency.Ban;
internationalLawBook.producerId = INQUISIDOR.id;
upgrades.push(internationalLawBook);

const mondialLawBook = new Upgrade();
mondialLawBook.name = {
  fr: 'Livre de lois mondiales'
};
mondialLawBook.description = {
  fr: 'Desc'
};
mondialLawBook.price = 100_000_000_000;
mondialLawBook.unlockValue = 75;
mondialLawBook.increase = new Big('0.70');
mondialLawBook.type = UpgradeType.DecreaseCost;
mondialLawBook.scope = UpgradeScope.Producer;
mondialLawBook.unlockCurrency = Currency.Producer;
mondialLawBook.priceCurrency = Currency.Ban;
mondialLawBook.producerId = INQUISIDOR.id;
upgrades.push(mondialLawBook);


const stellarLawBook = new Upgrade();
stellarLawBook.name = {
  fr: 'Livre de lois stellaires'
};
stellarLawBook.description = {
  fr: 'Desc'
};
stellarLawBook.price = 1_000_000_000_000;
stellarLawBook.unlockValue = 100;
stellarLawBook.increase = new Big('0.64');
stellarLawBook.type = UpgradeType.DecreaseCost;
stellarLawBook.scope = UpgradeScope.Producer;
stellarLawBook.unlockCurrency = Currency.Producer;
stellarLawBook.priceCurrency = Currency.Ban;
stellarLawBook.producerId = INQUISIDOR.id;
upgrades.push(stellarLawBook);

const galacticLawBook = new Upgrade();
galacticLawBook.name = {
  fr: 'Livre de lois galactiques'
};
galacticLawBook.description = {
  fr: 'Desc'
};
galacticLawBook.price = 10_000_000_000_000;
galacticLawBook.unlockValue = 125;
galacticLawBook.increase = new Big('0.58');
galacticLawBook.type = UpgradeType.DecreaseCost;
galacticLawBook.scope = UpgradeScope.Producer;
galacticLawBook.unlockCurrency = Currency.Producer;
galacticLawBook.priceCurrency = Currency.Ban;
galacticLawBook.producerId = INQUISIDOR.id;
upgrades.push(galacticLawBook);

const intergalacticLawBook = new Upgrade();
intergalacticLawBook.name = {
  fr: 'Livre de lois intergalactiques'
};
intergalacticLawBook.description = {
  fr: 'Desc'
};
intergalacticLawBook.price = 100_000_000_000_000;
intergalacticLawBook.unlockValue = 150;
intergalacticLawBook.increase = new Big('0.52');
intergalacticLawBook.type = UpgradeType.DecreaseCost;
intergalacticLawBook.scope = UpgradeScope.Producer;
intergalacticLawBook.unlockCurrency = Currency.Producer;
intergalacticLawBook.priceCurrency = Currency.Ban;
intergalacticLawBook.producerId = INQUISIDOR.id;
upgrades.push(intergalacticLawBook);


export { upgrades as INQUISIDOR_UPGRADES };
