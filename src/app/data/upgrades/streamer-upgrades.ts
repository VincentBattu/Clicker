import { Upgrade } from '../../../model/upgrades/upgrade';
import { UpgradeType } from '../../../model/upgrades/upgrade-type';
import { Currency } from '../../../model/upgrades/currency';
import { UpgradeScope } from '../../../model/upgrades/upgrade-scope';
import Big from 'big.js';
import { STREAMER } from '../producers/streamer';

const upgrades = [];

// ---------------------------------------------------------------------------------------------------------
//                                      AMELIORATION DU BPS
// ---------------------------------------------------------------------------------------------------------
const trackmania = new Upgrade();
trackmania.name = {
    fr: 'Trackmania'
};
trackmania.description = {
    fr: 'Parfait pour les soirées de détente ce jeu raviera petits et grands'
};
trackmania.price = 500_000;
trackmania.unlockValue = 10;
trackmania.increase = new Big('2');
trackmania.type = UpgradeType.IncreaseFlat;
trackmania.scope = UpgradeScope.Producer;
trackmania.unlockCurrency = Currency.Producer;
trackmania.priceCurrency = Currency.Ban;
trackmania.producerId = STREAMER.id;
upgrades.push(trackmania);

const minecraft = new Upgrade();
minecraft.name = {
    fr: 'Minecraft'
};
minecraft.description = {
    fr: `Certes c'est rageant de ne pas trouver de diamants après 1h de farming opti, mais bon ce jeu est beaucoup trop agréable`
};
minecraft.price = 1_000_000;
minecraft.unlockValue = 20;
minecraft.increase = new Big('2');
minecraft.type = UpgradeType.IncreaseFlat;
minecraft.scope = UpgradeScope.Producer;
minecraft.unlockCurrency = Currency.Producer;
minecraft.priceCurrency = Currency.Ban;
minecraft.producerId = STREAMER.id;
upgrades.push(minecraft);

const skyrim = new Upgrade();
skyrim.name = {
    fr: 'skyrim'
};
skyrim.description = {
    fr: 'skyrim toussa'
};
skyrim.price = 5_000_000;
skyrim.unlockValue = 35;
skyrim.increase = new Big('2');
skyrim.type = UpgradeType.IncreaseFlat;
skyrim.scope = UpgradeScope.Producer;
skyrim.unlockCurrency = Currency.Producer;
skyrim.priceCurrency = Currency.Ban;
skyrim.producerId = STREAMER.id;
upgrades.push(skyrim);

const hearthstone = new Upgrade();
hearthstone.name = {
    fr: 'Hearthstone'
};
hearthstone.description = {
    fr: `C'est amusant de se faire OTK par un freeze à 1HP alors qu'on en à 70 n'est ce pas ?`
};
hearthstone.price = 10_000_000;
hearthstone.unlockValue = 50;
hearthstone.increase = new Big('2');
hearthstone.type = UpgradeType.IncreaseFlat;
hearthstone.scope = UpgradeScope.Producer;
hearthstone.unlockCurrency = Currency.Producer;
hearthstone.priceCurrency = Currency.Ban;
hearthstone.producerId = STREAMER.id;
upgrades.push(hearthstone);

const darkSouls = new Upgrade();
darkSouls.name = {
    fr: 'Dark Souls'
};
darkSouls.description = {
    fr: 'Mais si le jeu est facile, il suffit de faire des petites roulades tout le temps'
};
darkSouls.price = 50_000_000;
darkSouls.unlockValue = 75;
darkSouls.increase = new Big('2');
darkSouls.type = UpgradeType.IncreaseFlat;
darkSouls.scope = UpgradeScope.Producer;
darkSouls.unlockCurrency = Currency.Producer;
darkSouls.priceCurrency = Currency.Ban;
darkSouls.producerId = STREAMER.id;
upgrades.push(darkSouls);

const fortnite = new Upgrade();
fortnite.name = {
    fr: 'Fortnite'
};
fortnite.description = {
    fr: 'Encore un jeu avec de gamins de 12ans de partout, ils vont mettre leurs top 1 en story après'
};
fortnite.price = 200_000_000;
fortnite.unlockValue = 100;
fortnite.increase = new Big('3');
fortnite.type = UpgradeType.IncreaseFlat;
fortnite.scope = UpgradeScope.Producer;
fortnite.unlockCurrency = Currency.Producer;
fortnite.priceCurrency = Currency.Ban;
fortnite.producerId = STREAMER.id;
upgrades.push(fortnite);

const lol = new Upgrade();
lol.name = {
    fr: 'LoL'
};
lol.description = {
    fr: `Vraiment la meilleure des communautés, il est extrêmement difficile de s'énerver `
};
lol.price = 800_000_000;
lol.unlockValue = 125;
lol.increase = new Big('2');
lol.type = UpgradeType.IncreaseFlat;
lol.scope = UpgradeScope.Producer;
lol.unlockCurrency = Currency.Producer;
lol.priceCurrency = Currency.Ban;
lol.producerId = STREAMER.id;
upgrades.push(lol);

const gettingOverIt = new Upgrade();
gettingOverIt.name = {
    fr: 'Getting Over It'
};
gettingOverIt.description = {
    fr: 'Un gars énorme et sec avec un marteau dans une soupière, la recette parfaite pour dominer le twitch game'
};
gettingOverIt.price = 2_000_000_000;
gettingOverIt.unlockValue = 150;
gettingOverIt.increase = new Big('2');
gettingOverIt.type = UpgradeType.IncreaseFlat;
gettingOverIt.scope = UpgradeScope.Producer;
gettingOverIt.unlockCurrency = Currency.Producer;
gettingOverIt.priceCurrency = Currency.Ban;
gettingOverIt.producerId = STREAMER.id;
upgrades.push(gettingOverIt);



// ---------------------------------------------------------------------------------------------------------
//                                      Réduction du coût
// ---------------------------------------------------------------------------------------------------------
const scream = new Upgrade();
scream.name = {
    fr: 'Hurler'
};
scream.description = {
    fr: `Faites attention, votre voisin c'est le genre de gars qui se couche à 21h30, vous allez quand même pas le réveiller`
};
scream.price = 500_000;
scream.unlockValue = 10;
scream.increase = new Big('0.94');
scream.type = UpgradeType.DecreaseCost;
scream.scope = UpgradeScope.Producer;
scream.unlockCurrency = Currency.Producer;
scream.priceCurrency = Currency.Ban;
scream.producerId = STREAMER.id;
upgrades.push(scream);

const hitDesktop = new Upgrade();
hitDesktop.name = {
    fr: 'Frapper sur le bureau'
};
hitDesktop.description = {
    fr: 'Ma lumière ! Va falloir réparer tout ça'
};
hitDesktop.price = 1_500_000;
hitDesktop.unlockValue = 20;
hitDesktop.increase = new Big('0.88');
hitDesktop.type = UpgradeType.DecreaseCost;
hitDesktop.scope = UpgradeScope.Producer;
hitDesktop.unlockCurrency = Currency.Producer;
hitDesktop.priceCurrency = Currency.Ban;
hitDesktop.producerId = STREAMER.id;
upgrades.push(hitDesktop);

const changeDesktop = new Upgrade();
changeDesktop.name = {
    fr: 'Changer le bureau'
};
changeDesktop.description = {
    fr: `Bah voilà, c'était difficile de jouer avec un trou dedans`
};
changeDesktop.price = 7_500_000;
changeDesktop.unlockValue = 35;
changeDesktop.increase = new Big('0.82');
changeDesktop.type = UpgradeType.DecreaseCost;
changeDesktop.scope = UpgradeScope.Producer;
changeDesktop.unlockCurrency = Currency.Producer;
changeDesktop.priceCurrency = Currency.Ban;
changeDesktop.producerId = STREAMER.id;
upgrades.push(changeDesktop);

const takeABath = new Upgrade();
takeABath.name = {
    fr: 'Prendre un bain'
};
takeABath.description = {
    fr: `Pour se reposer d'une bonne journée de ban, et parce que vous avez passer 3 jours enfermé dans votre chambre... Sérieux l'odeur est
     horrible`
};
takeABath.price = 20_000_000;
takeABath.unlockValue = 50;
takeABath.increase = new Big('0.76');
takeABath.type = UpgradeType.DecreaseCost;
takeABath.scope = UpgradeScope.Producer;
takeABath.unlockCurrency = Currency.Producer;
takeABath.priceCurrency = Currency.Ban;
takeABath.producerId = STREAMER.id;
upgrades.push(takeABath);

const kebab = new Upgrade();
kebab.name = {
    fr: 'Acheter un kebab STO'
};
kebab.description = {
    fr: `Pour seulement 5€, c'est le repas préféré du gamer lambda`
};
kebab.price = 100_000_000;
kebab.unlockValue = 75;
kebab.increase = new Big('0.70');
kebab.type = UpgradeType.DecreaseCost;
kebab.scope = UpgradeScope.Producer;
kebab.unlockCurrency = Currency.Producer;
kebab.priceCurrency = Currency.Ban;
kebab.producerId = STREAMER.id;
upgrades.push(kebab);

const beach = new Upgrade();
beach.name = {
    fr: 'Aller à la plage'
};
beach.description = {
    fr: 'Vous sortez dehrors ! Faites attention le soleil brûle votre peau en 0.0014 seconde'
};
beach.price = 800_000_000;
beach.unlockValue = 100;
beach.increase = new Big('0.64');
beach.type = UpgradeType.DecreaseCost;
beach.scope = UpgradeScope.Producer;
beach.unlockCurrency = Currency.Producer;
beach.priceCurrency = Currency.Ban;
beach.producerId = STREAMER.id;
upgrades.push(beach);

const startWar = new Upgrade();
startWar.name = {
    fr: 'Démarrer une guerre'
};
startWar.description = {
    fr: `Ca y'est vous êtes dans le putaclick, et vous vous mettez à clasher des youtubers de manière très aléatoire`
};
startWar.price = 2_000_000_000;
startWar.unlockValue = 125;
startWar.increase = new Big('0.58');
startWar.type = UpgradeType.DecreaseCost;
startWar.scope = UpgradeScope.Producer;
startWar.unlockCurrency = Currency.Producer;
startWar.priceCurrency = Currency.Ban;
startWar.producerId = STREAMER.id;
upgrades.push(startWar);

const stopLol = new Upgrade();
stopLol.name = {
    fr: 'Arrêter LoL'
};
stopLol.description = {
    fr: `C'EST UN SCANDALE ! Toute votre communauté viens vous lacher des commentaires de rageux dans voter chat, vous allez pouvoir vous am
    user`
};
stopLol.price = 10_000_000_000;
stopLol.unlockValue = 150;
stopLol.increase = new Big('0.52');
stopLol.type = UpgradeType.DecreaseCost;
stopLol.scope = UpgradeScope.Producer;
stopLol.unlockCurrency = Currency.Producer;
stopLol.priceCurrency = Currency.Ban;
stopLol.producerId = STREAMER.id;
upgrades.push(stopLol);

export { upgrades as STREAMER_UPGRADES };
