import { Upgrade } from '../../../model/upgrades/upgrade';
import { UpgradeType } from '../../../model/upgrades/upgrade-type';
import { UpgradeScope } from '../../../model/upgrades/upgrade-scope';
import { Currency } from '../../../model/upgrades/currency';
import Big from 'big.js';
import { PUNISHER } from '../producers/punisher';

const upgrades = [];

// ---------------------------------------------------------------------------------------------------------
//                                      AMELIORATION DU BPS
// ---------------------------------------------------------------------------------------------------------
const penality = new Upgrade();
penality.name = {
    fr: 'Pénalité'
};
penality.description = {
    fr: `L'ustensile indispensable`
};
penality.price = 800_000;
penality.unlockValue = 10;
penality.increase = new Big('2');
penality.type = UpgradeType.IncreaseFlat;
penality.scope = UpgradeScope.Producer;
penality.unlockCurrency = Currency.Producer;
penality.priceCurrency = Currency.Ban;
penality.producerId = PUNISHER.id;
upgrades.push(penality);

const sanction = new Upgrade();
sanction.name = {
    fr: 'Sanction'
};
sanction.description = {
    fr: 'A la mode depuis Louis XIV'
};
sanction.price = 8_000_000;
sanction.unlockValue = 20;
sanction.increase = new Big('2');
sanction.type = UpgradeType.IncreaseFlat;
sanction.scope = UpgradeScope.Producer;
sanction.unlockCurrency = Currency.Producer;
sanction.priceCurrency = Currency.Ban;
sanction.producerId = PUNISHER.id;
upgrades.push(sanction);

const punition = new Upgrade();
punition.name = {
    fr: 'Punition'
};
punition.description = {
    fr: `Silence s'il vous plaît ! La on parle de respect`
};
punition.price = 80_000_000;
punition.unlockValue = 35;
punition.increase = new Big('2');
punition.type = UpgradeType.IncreaseFlat;
punition.scope = UpgradeScope.Producer;
punition.unlockCurrency = Currency.Producer;
punition.priceCurrency = Currency.Ban;
punition.producerId = PUNISHER.id;
upgrades.push(punition);

const repression = new Upgrade();
repression.name = {
  fr: 'Répression'
};
repression.description = {
  fr: 'Desc'
};
repression.price = 800_000_000;
repression.unlockValue = 50;
repression.increase = new Big('2');
repression.type = UpgradeType.IncreaseFlat;
repression.scope = UpgradeScope.Producer;
repression.unlockCurrency = Currency.Producer;
repression.priceCurrency = Currency.Ban;
repression.producerId = PUNISHER.id;
upgrades.push(repression);

const retribution = new Upgrade();
retribution.name = {
  fr: 'Châtiment'
};
retribution.description = {
  fr: 'Desc'
};
retribution.price = 8_000_000_000;
retribution.unlockValue = 75;
retribution.increase = new Big('2');
retribution.type = UpgradeType.IncreaseFlat;
retribution.scope = UpgradeScope.Producer;
retribution.unlockCurrency = Currency.Producer;
retribution.priceCurrency = Currency.Ban;
retribution.producerId = PUNISHER.id;
upgrades.push(retribution);

const ordeal = new Upgrade();
ordeal.name = {
  fr: 'Supplice'
};
ordeal.description = {
  fr: 'Desc'
};
ordeal.price = 80_000_000_000;
ordeal.unlockValue = 100;
ordeal.increase = new Big('3');
ordeal.type = UpgradeType.IncreaseFlat;
ordeal.scope = UpgradeScope.Producer;
ordeal.unlockCurrency = Currency.Producer;
ordeal.priceCurrency = Currency.Ban;
ordeal.producerId = PUNISHER.id;
upgrades.push(ordeal);

const torture = new Upgrade();
torture.name = {
  fr: 'Torture'
};
torture.description = {
  fr: 'Desc'
};
torture.price = 800_000_000_000;
torture.unlockValue = 125;
torture.increase = new Big('2');
torture.type = UpgradeType.IncreaseFlat;
torture.scope = UpgradeScope.Producer;
torture.unlockCurrency = Currency.Producer;
torture.priceCurrency = Currency.Ban;
torture.producerId = PUNISHER.id;
upgrades.push(torture);

const agony = new Upgrade();
agony.name = {
  fr: 'Agonie'
};
agony.description = {
  fr: 'Desc'
};
agony.price = 80_000_000_000_000;
agony.unlockValue = 150;
agony.increase = new Big('2');
agony.type = UpgradeType.IncreaseFlat;
agony.scope = UpgradeScope.Producer;
agony.unlockCurrency = Currency.Producer;
agony.priceCurrency = Currency.Ban;
agony.producerId = PUNISHER.id;
upgrades.push(agony);


// ---------------------------------------------------------------------------------------------------------
//                                      Réduction du coût
// ---------------------------------------------------------------------------------------------------------

const bigGun = new Upgrade();
bigGun.name = {
    fr: 'Gros flingue'
};
bigGun.description = {
    fr: `L'arme typique des mecs bourrés de testostérone`
};
bigGun.price = 75_000;
bigGun.unlockValue = 10;
bigGun.increase = new Big('0.94');
bigGun.type = UpgradeType.DecreaseCost;
bigGun.scope = UpgradeScope.Producer;
bigGun.unlockCurrency = Currency.Producer;
bigGun.priceCurrency = Currency.Ban;
bigGun.producerId = PUNISHER.id;
upgrades.push(bigGun);

const veryBigGun = new Upgrade();
veryBigGun.name = {
    fr: 'Très gros flingue'
};
veryBigGun.description = {
    fr: 'Toujours dans la démesure'
};
veryBigGun.price = 10_000_000;
veryBigGun.unlockValue = 20;
veryBigGun.increase = new Big('0.88');
veryBigGun.type = UpgradeType.DecreaseCost;
veryBigGun.scope = UpgradeScope.Producer;
veryBigGun.unlockCurrency = Currency.Producer;
veryBigGun.priceCurrency = Currency.Ban;
veryBigGun.producerId = PUNISHER.id;
upgrades.push(veryBigGun);

const tooBigGun = new Upgrade();
tooBigGun.name = {
    fr: 'Flingue vraiment trop gros'
};
tooBigGun.description = {
    fr: `C'est que ça commence a être un peu lourd à porter là`
};
tooBigGun.price = 100_000_000;
tooBigGun.unlockValue = 35;
tooBigGun.increase = new Big('0.82');
tooBigGun.type = UpgradeType.DecreaseCost;
tooBigGun.scope = UpgradeScope.Producer;
tooBigGun.unlockCurrency = Currency.Producer;
tooBigGun.priceCurrency = Currency.Ban;
tooBigGun.producerId = PUNISHER.id;
upgrades.push(tooBigGun);

const inhumanGun = new Upgrade();
inhumanGun.name = {
  fr: `Flingue d'une taille déraisonnable pour un humain normalement constitué`
};
inhumanGun.description = {
  fr: `Alors là chapeau, se trimballer avec un truc de cette taille, nan mais vous avez vu ?!`
};
inhumanGun.price = 10_000_000_000;
inhumanGun.unlockValue = 50;
inhumanGun.increase = new Big('0.76');
inhumanGun.type = UpgradeType.DecreaseCost;
inhumanGun.scope = UpgradeScope.Producer;
inhumanGun.unlockCurrency = Currency.Producer;
inhumanGun.priceCurrency = Currency.Ban;
inhumanGun.producerId = PUNISHER.id;
upgrades.push(inhumanGun);

const waterGun = new Upgrade();
waterGun.name = {
  fr: 'Pistolet à eau nucléaire'
};
waterGun.description = {
  fr: `Une trouvaille estivale qui va animer vos soirées piscines`
};
waterGun.price = 100_000_000_000;
waterGun.unlockValue = 75;
waterGun.increase = new Big('0.70');
waterGun.type = UpgradeType.DecreaseCost;
waterGun.scope = UpgradeScope.Producer;
waterGun.unlockCurrency = Currency.Producer;
waterGun.priceCurrency = Currency.Ban;
waterGun.producerId = PUNISHER.id;
upgrades.push(waterGun);


const banzooka = new Upgrade();
banzooka.name = {
  fr: 'Banzooka'
};
banzooka.description = {
  fr: `Vous imposer le respect à l'américaine, en éliminant des armées à coup de ban`
};
banzooka.price = 1_000_000_000_000;
banzooka.unlockValue = 100;
banzooka.increase = new Big('0.64');
banzooka.type = UpgradeType.DecreaseCost;
banzooka.scope = UpgradeScope.Producer;
banzooka.unlockCurrency = Currency.Producer;
banzooka.priceCurrency = Currency.Ban;
banzooka.producerId = PUNISHER.id;
upgrades.push(banzooka);

const rpg = new Upgrade();
rpg.name = {
  fr: 'Lance roquette RPGX42'
};
rpg.description = {
  fr: `La dernière trouvaille du professeur B, capable de détruire une planète sans munition`
};
rpg.price = 10_000_000_000_000;
rpg.unlockValue = 125;
rpg.increase = new Big('0.58');
rpg.type = UpgradeType.DecreaseCost;
rpg.scope = UpgradeScope.Producer;
rpg.unlockCurrency = Currency.Producer;
rpg.priceCurrency = Currency.Ban;
rpg.producerId = PUNISHER.id;
upgrades.push(rpg);

const bigPunch = new Upgrade();
bigPunch.name = {
  fr: 'Grosses mandales'
};
bigPunch.description = {
  fr: `Bon ça suffit les conneries, tout ça c'était bien beau, mais il faut savoir revenir à la base, se battre comme un HOMME`
};
bigPunch.price = 100_000_000_000_000;
bigPunch.unlockValue = 150;
bigPunch.increase = new Big('0.52');
bigPunch.type = UpgradeType.DecreaseCost;
bigPunch.scope = UpgradeScope.Producer;
bigPunch.unlockCurrency = Currency.Producer;
bigPunch.priceCurrency = Currency.Ban;
bigPunch.producerId = PUNISHER.id;
upgrades.push(bigPunch);


export { upgrades as PUNISHER_UPGRADES };
