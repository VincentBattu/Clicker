import { Producer } from '../../../model/producer';
import Big from 'big.js';

const bot = new Producer();
bot.id = 1;
bot.name = 'Bot';
bot.description = 'Un super bot';
bot.basePrice = new Big(150);
bot.production = new Big('1');

export { bot as BOT };
