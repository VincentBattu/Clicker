import { Producer } from '../../../model/producer';
import Big from 'big.js';

const judge = new Producer();
judge.id = 2;
judge.name = 'Juge';
judge.description = 'Un super juge';
judge.basePrice = new Big(3_000);
judge.production = new Big(10);

export { judge as JUDGE };
