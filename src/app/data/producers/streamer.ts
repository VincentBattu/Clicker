import { Producer } from '../../../model/producer';
import Big from 'big.js';

const streamer = new Producer();
streamer.id = 3;
streamer.name = 'Streamer';
streamer.description = 'Un super streamer';
streamer.basePrice = new Big(17_000);
streamer.production = new Big(100);

export { streamer as STREAMER };
