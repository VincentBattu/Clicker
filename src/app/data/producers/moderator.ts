import { Producer } from '../../../model/producer';
import Big from 'big.js';

const moderator = new Producer();
moderator.id = 0;
moderator.name = 'Modérateur';
moderator.description = 'Un super modérateur';
moderator.basePrice = new Big(10);
moderator.production = new Big('0.1');

export { moderator as MODERATOR };
