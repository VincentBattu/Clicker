import { Producer } from '../../../model/producer';
import Big from 'big.js';

const twoGods = new Producer();
twoGods.id = 7;
twoGods.name = 'Deux dieux';
twoGods.description = 'Deux super dieux';
twoGods.basePrice = new Big(2_000_000_000);
twoGods.production = new Big(1_000_000);

export { twoGods as TWO_GODS };
