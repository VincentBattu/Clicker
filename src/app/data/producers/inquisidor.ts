import { Producer } from '../../../model/producer';
import Big from 'big.js';

const inquisidor = new Producer();
inquisidor.id = 4;
inquisidor.name = 'Inquisiteur';
inquisidor.description = 'Un super inquisiteur';
inquisidor.basePrice = new Big(600_000);
inquisidor.production = new Big(1_000);

export { inquisidor as INQUISIDOR };
