import { Producer } from '../../../model/producer';
import Big from 'big.js';

const punisher = new Producer();
punisher.id = 5;
punisher.name = 'Punisher';
punisher.description = 'Un super punisher';
punisher.basePrice = new Big(1_500_000);
punisher.production = new Big(10_000);

export { punisher as PUNISHER };
