import { Producer } from '../../../model/producer';
import Big from 'big.js';

const god = new Producer();
god.id = 6;
god.name = 'Dieu';
god.description = 'Un super dieu';
god.basePrice = new Big(50_000_000);
god.production = new Big(100_000);

export { god as GOD };
