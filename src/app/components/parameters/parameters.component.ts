import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Parameters, ParametersStore } from '../../../store/store/parameters-store.service';
import { Counts, CountsStore } from '../../../store/store/counts-store.service';
import { RoundingMode } from 'big.js';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-parameters',
  templateUrl: './parameters.component.html',
  styleUrls: ['./parameters.component.css']
})
export class ParametersComponent implements OnInit, OnDestroy {

  /**
   * Souscription au store des paramètre
   */
  private parametersSubscription: Subscription;

  /**
   * Souscription au store des compteurs
   */
  private countsSubscription: Subscription;

  /**
   * Objet partagé par le store
   */
  public parameters: Parameters;

  /**
   * Compteurs partagés
   */
  public counts: Counts;

  @Output()
  public closeParameters = new EventEmitter<void>();

  /**
   * Mode d'arrondi
   * @type {RoundingMode.RoundDown}
   */
  public roundingMode = RoundingMode.RoundDown;


  constructor (private parametersStore: ParametersStore,
               private countsStore: CountsStore) {
  }

  ngOnInit () {
    // On souscrit aux changements des stores
    this.parametersSubscription = this.parametersStore.state.subscribe(p => this.parameters = p);
    this.countsSubscription = this.countsStore.state.subscribe(c => this.counts = c);
  }

  ngOnDestroy () {
    // On se désabonne aux changements des stores
    this.parametersSubscription.unsubscribe();
    this.countsSubscription.unsubscribe();
  }

  public onCloseParameters () {
    this.closeParameters.emit();
  }

  public ngModelGameNameChange (gameName: string) {
    this.parametersStore.gameName = gameName;
  }

}
