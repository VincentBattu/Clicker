import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Producer } from '../../../model/producer';
import { Counts, CountsStore } from '../../../store/store/counts-store.service';
import { ProducersStore } from '../../../store/store/producers-store.service';

@Component({
  selector: 'app-producers',
  templateUrl: './producers.component.html',
  styleUrls: ['./producers.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProducersComponent implements OnInit, OnDestroy {

  /**
   * Abonnement aux changements des producteurs
   */
  private producersSubscription: Subscription;

  /**
   * Abonnement aux changements des compteurs
   */
  private countsSubscription: Subscription;

  /**
   * Liste des producteurs
   */
  public producers: Producer[];

  /**
   * Compteurs
   */
  public counts: Counts;

  constructor (private producerStore: ProducersStore,
               private countsStore: CountsStore,
               private cdr: ChangeDetectorRef) {
  }

  ngOnInit (): void {
    // On s'abonnes aux changements des stores
    this.producersSubscription = this.producerStore.state.subscribe(p => {
      this.producers = p;
      this.cdr.detectChanges();
    });
    this.countsSubscription = this.countsStore.state.subscribe(c => {
      this.counts = c;
      this.cdr.detectChanges();
    });
  }

  ngOnDestroy (): void {
    // On se désabonne des changements des stores
    this.producersSubscription.unsubscribe();
    this.countsSubscription.unsubscribe();
  }

}
