import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Producer } from '../../../model/producer';
import { RoundingMode } from 'big.js';
import { FormatBigNumberPipe } from '../../pipes/format-big-number.pipe';
import Big from 'big.js';
import { CountsStore } from '../../../store/store/counts-store.service';
import { ProducersStore } from '../../../store/store/producers-store.service';
import { ParametersStore } from '../../../store/store/parameters-store.service';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-producer',
  templateUrl: './producer.component.html',
  styleUrls: ['./producer.component.css'],
})
export class ProducerComponent implements OnInit, OnDestroy {

  /**
   * Producteur concerné
   */
  @Input()
  public producer: Producer;

  /**
   * Compteur actuel
   */
  @Input()
  public count: Big;

  /**
   * Mode d'arrondi
   * @type {RoundingMode.RoundUp}
   */
  public roundingUp = RoundingMode.RoundUp;

  /**
   * Indique un level up
   */
  public canLevelUp = '';

  /**
   * Niveau max avant de level up
   */
  public maxLevel = 150;

  /**
   * Augmentation de la production par level up
   */
  public multiplyFactor = 1000;


  constructor (private formatBigBumberPipe: FormatBigNumberPipe,
               private producerStore: ProducersStore,
               private countsStore: CountsStore,
               public parametersStore: ParametersStore) {
  }

  ngOnInit (): void {
    // On s'abonne aux changements des stores
  }

  ngOnDestroy (): void {
    // On se désabonne des changements des stores
  }

  /**
   * A l'achat d'un producteur
   *
   * @param {MouseEvent} e
   */
  public buy (e: MouseEvent): void {

    e.preventDefault();
    e.stopPropagation();

    if (this.count.gte(this.producer.price)) {
      // On diminue le compteur
      this.countsStore.decreaseCount(this.producer.price);

      // On change le nombre de producteur
      this.manageLevelUp();

      // On change le producteur
      this.producerStore.update(this.producer);

      // On incrémente la production par seconde
      this.parametersStore.computePerSecond(this.producerStore.value);
    }
  }


  public buyMax (e: MouseEvent): void {
    e.preventDefault();
    e.stopPropagation();

    let max = this.computeMaxProducers();

    if (max >= this.maxLevel || max + this.producer.number > this.maxLevel) {
      max = this.maxLevel - this.producer.number;
      this.showCanLevelUp();
    }

    const price = new Big(((Math.pow(1.15, max) - 1) / 0.15) * +this.producer.basePrice.mul(new Big(1.15).pow(this.producer.number)));

    this.countsStore.decreaseCount(price);
    this.producer.number += max;

    this.producerStore.update(this.producer);

    this.parametersStore.computePerSecond(this.producerStore.value);

  }

  /**
   * Retourne le maximum de producteurs achetable avec le compteur actuel
   * @returns {number}
   */
  public computeMaxProducers (): number {
    return Math.floor((Math.log(+(this.count.mul(0.15)
      .div(this.producer.basePrice.mul(new Big(1.15).pow(this.producer.number)))).add(1)) / Math.log(1.15)));
  }

  /**
   * Permet de faire level up un producer et de gérer leur nombre
   */
  private manageLevelUp (): void {
    if (this.producer.number === this.maxLevel) {
      this.producer.number = 1;
      this.producer.production = this.producer.production.mul(this.multiplyFactor);
      this.canLevelUp = '';
      this.producer.level = this.producer.level + 1;
      this.producer.basePrice = this.producer.basePrice.mul(100);
    } else {
      this.producer.number += 1;
      if (this.producer.number === this.maxLevel) {
        this.showCanLevelUp();
      }
    }
  }

  /**
   * Informe l'utilisateur qu'il peut level up
   */
  private showCanLevelUp () {
    this.canLevelUp = 'lightblue';
  }
}
