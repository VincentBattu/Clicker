import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Upgrade } from '../../../model/upgrades/upgrade';
import { Currency } from '../../../model/upgrades/currency';
import { UpgradesStore } from '../../../store/store/upgrades-store.service';
import { ProducersStore } from '../../../store/store/producers-store.service';
import { Counts, CountsStore } from '../../../store/store/counts-store.service';

@Component({
  selector: 'app-upgrades',
  templateUrl: './upgrades.component.html',
  styleUrls: ['./upgrades.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpgradesComponent implements OnInit, OnDestroy {

  /**
   * Abonnement aux changements des améliorations
   */
  private upgradesSubscription: Subscription;

  /**
   * Abonnement aux changements des producteurs
   */
  private producersSubscription: Subscription;

  /**
   * Abonnementx aux changements des compteurs
   */
  private countsSubscription: Subscription;

  /**
   * Améliorations
   */
  public upgrades: Upgrade[];

  /**
   * Compteurs
   */
  public counts: Counts;

  constructor (private upgradesStore: UpgradesStore,
               private producersStore: ProducersStore,
               private countsStore: CountsStore,
               private cdr: ChangeDetectorRef) {
  }

  ngOnInit () {
    this.upgradesSubscription = this.upgradesStore.state.subscribe(u => {
      this.upgrades = u;
      this.cdr.detectChanges();
    });

    this.countsSubscription = this.countsStore.state.subscribe(c => {
      this.counts = c;
      this.cdr.detectChanges();
    });

    // On relance la mise à jour du composant (et de ses composant enfants) uniquement aux changements dans les producteurs
    this.producersSubscription = this.producersStore.state.subscribe(() => this.cdr.detectChanges());
  }

  ngOnDestroy (): void {
    this.upgradesSubscription.unsubscribe();
    this.producersSubscription.unsubscribe();
    this.countsSubscription.unsubscribe();
  }

  /**
   * Définit si une upgrade est affichée ou non.
   *
   * @param {Upgrade} upgrade
   * @returns {boolean}
   */
  public isVisible (upgrade: Upgrade): boolean {

    // Si l'amélioration a déjà été débloquée, on ne l'affiche pas
    if (upgrade.unlocked) {
      return false;
    }

    const producers = this.producersStore.value;

    switch (upgrade.unlockCurrency) {
      // La devise est le producteur
      case Currency.Producer:
        return upgrade.unlockValue <= producers[upgrade.producerId].number;
      // La devise est la ban
      case Currency.Ban:
        return this.counts.totalCount.gte(upgrade.unlockValue);
      // La devise est le click (actuel)
      case Currency.Click:
        return false;
      // Ne doit pas arriver
      default:
        return false;
    }
  }

}
