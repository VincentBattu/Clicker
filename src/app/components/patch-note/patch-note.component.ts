import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-patch-note',
  templateUrl: './patch-note.component.html',
  styleUrls: ['./patch-note.component.css']
})
export class PatchNoteComponent implements OnInit, OnDestroy {

  @Output()
  public closePatchNote = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  public onClosePatchNote() {
    this.closePatchNote.emit();
  }

}
