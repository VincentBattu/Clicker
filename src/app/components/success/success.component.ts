import { Subscription } from 'rxjs';
import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Parameters, ParametersStore } from '../../../store/store/parameters-store.service';
import { Counts, CountsStore } from '../../../store/store/counts-store.service';
import { ProducersStore } from '../../../store/store/producers-store.service';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit, OnDestroy {

   /**
   * Souscription au store des paramètre
   */
  private parametersSubscription: Subscription;

  /**
   * Souscription au store des compteurs
   */
  private countsSubscription: Subscription;

  /**
   * Objet partagé par le store
   */
  public parameters: Parameters;

  /**
   * Compteurs partagés
   */
  public counts: Counts;

  @Output()
  public closeSuccess = new EventEmitter<void>();

  constructor(private producerStore: ProducersStore,
              private countsStore: CountsStore,
              private parametersStore: ParametersStore) { }

  ngOnInit() {
     // On souscrit aux changements des stores
     this.parametersSubscription = this.parametersStore.state.subscribe(p => this.parameters = p);
     this.countsSubscription = this.countsStore.state.subscribe(c => this.counts = c);
  }

  ngOnDestroy(): void {
    // On se désabonne aux changements des stores
    this.parametersSubscription.unsubscribe();
    this.countsSubscription.unsubscribe();
  }

  public onCloseSuccess () {
    this.closeSuccess.emit();
  }

}
