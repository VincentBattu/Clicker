import { Component, OnInit } from '@angular/core';
import { BAN_MESSAGES } from '../../data/ban-message';
import { PSEUDOS } from '../../data/pseudos';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  /**
   * Couleurs disponibles pour les pseudos
   * @type {string[]}
   */
  private static readonly COLORS = [
    '#ff9ff3',
    '#feca57',
    '#ee5253',
    '#5f27cd',
    '#0abde3',
    '#01a3a4',
    '#00b894',
    '#f39c12',
    '#0652DD',
    '#6F1E51',
    '#ff6b81',
    '#f78fb3',
    '#BDC581',
    '#009432',
    '#535c68'
  ];

  /**
   * Délai minimal pour l'affichage du prochain message (en secondes)
   * @type {number}
   */
  private static readonly MIN_DELAY = 2_000;

  /**
   * Délai maximal pour l'affichage du prochain message (en secondes)
   * @type {number}
   */
  private static readonly MAX_DELAY = 7_000;

  /**
   * Messages possibles
   * @type {string[]}
   */
  private banMessages = BAN_MESSAGES;

  /**
   * Pseudos possibles
   * @type {string[]}
   */
  private pseudos = PSEUDOS;

  /**
   * Messages affichés
   * @type {any[]}
   */
  public messages: Array<{ content: string, pseudo: string, date: Date }> = [];

  /**
   * Association de couleur à un pseudo
   * @type {Map<string, string>}
   */
  public nameToColor = new Map<string, string>();

  constructor() {
  }

  ngOnInit() {
    for (const pseudo of this.pseudos) {
      if (ChatComponent.COLORS.length > 0) {
        const i = Math.floor(Math.random() * ChatComponent.COLORS.length);
        this.nameToColor[pseudo] = ChatComponent.COLORS[i];
        ChatComponent.COLORS.splice(i, 1);
      } else {
        this.nameToColor[pseudo] = '#000';
      }

    }
    this.displayMessage();
  }


  private displayMessage() {
    if (this.messages.length === 13) {
      this.messages.pop();
    }
    this.messages.unshift({ content: this.getMessage(), pseudo: this.getPseudo(), date: new Date() });
    setTimeout(() => {
      this.displayMessage();
    }, Math.floor(Math.random() * (ChatComponent.MAX_DELAY - ChatComponent.MIN_DELAY)) + ChatComponent.MIN_DELAY);
  }

  /**
   * Retourne aléatoirement un message
   * @returns {string}
   */
  private getMessage(): string {
    return this.banMessages[Math.floor(Math.random() * this.banMessages.length)];
  }

  /**
   * Retourne aléatoirement un pseudo
   * @returns {string}
   */
  private getPseudo(): string {
    return this.pseudos[Math.floor(Math.random() * this.pseudos.length)];
  }

}
