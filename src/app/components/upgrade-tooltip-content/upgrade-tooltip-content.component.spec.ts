import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpgradeTooltipContentComponent } from './upgrade-tooltip-content.component';

describe('UpgradeTooltipContentComponent', () => {
  let component: UpgradeTooltipContentComponent;
  let fixture: ComponentFixture<UpgradeTooltipContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpgradeTooltipContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpgradeTooltipContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
