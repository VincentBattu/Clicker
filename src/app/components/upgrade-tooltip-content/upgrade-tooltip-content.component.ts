import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { UpgradeScope } from '../../../model/upgrades/upgrade-scope';
import { UpgradeType } from '../../../model/upgrades/upgrade-type';
import { Upgrade } from '../../../model/upgrades/upgrade';
import { Producer } from '../../../model/producer';
import { ProducersStore } from '../../../store/store/producers-store.service';
import Big from 'big.js';

@Component({
  selector: 'app-upgrade-tooltip-content',
  templateUrl: './upgrade-tooltip-content.component.html',
  styleUrls: ['./upgrade-tooltip-content.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpgradeTooltipContentComponent implements OnInit {

  /**
   * Amélioration concernée
   */
  @Input()
  public upgrade: Upgrade;

  /**
   * Type d'amélioration (utile dans le template)
   * @type {UpgradeType}
   */
  public upgradeType = UpgradeType;

  /**
   * Portée de l'amélioration (utile dans le template)
   * @type {UpgradeScope}
   */
  public upgradeScope = UpgradeScope;

  /**
   * Liste des producteurs. Pour l'instant, uniquement utilisé pour récupérer le nom,
   * on ne suit pas les changements des producteurs dans ce composant
   */
  public producers: Producer[];

  /**
   * Constante 1
   * @type {Big}
   */
  public one = new Big(1);

  constructor (private producersStore: ProducersStore) {
    this.producers = this.producersStore.value;
  }

  ngOnInit () {
  }

}
