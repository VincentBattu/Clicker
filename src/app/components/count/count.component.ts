import { Component, OnDestroy, OnInit } from '@angular/core';
import { RoundingMode } from 'big.js';
import { Subscription } from 'rxjs';
import { Parameters, ParametersStore } from '../../../store/store/parameters-store.service';
import { Counts, CountsStore } from '../../../store/store/counts-store.service';

@Component({
  selector: 'app-count',
  templateUrl: './count.component.html',
  styleUrls: ['./count.component.css']
})
export class CountComponent implements OnInit, OnDestroy {

  /**
   * Souscription aux changements du store des compteurs
   */
  private countsSubscription: Subscription;

  /**
   * Intervalle permettant d'augmenter les compteurs
   */
  private countInterval: number;

  /**
   * Souscription aux changements du store des paramètres
   */
  private parametersSubscription: Subscription;

  /**
   * Paramètres partagés
   */
  public parameters: Parameters;

  /**
   * Compteurs partagés
   */
  public counts: Counts;

  /**
   * Mode d'arrondi
   */
  public roundingMode = RoundingMode.RoundDown;

  constructor (private countsStore: CountsStore,
               private parametersStore: ParametersStore) {
  }

  ngOnInit (): void {
    // Souscription aux changements des stores
    this.countsSubscription = this.countsStore.state.subscribe(c => this.counts = c);
    this.parametersSubscription = this.parametersStore.state.subscribe(p => this.parameters = p);

    // On lance la boucle mettant à jour les compteurs
    this.countInterval = setInterval(() => {
      this.countsStore.increaseCount(this.parameters.perSecond);
    }, 1_000);
  }

  ngOnDestroy (): void {
    // Désabonnements aux changements des stores
    this.countsSubscription.unsubscribe();
    this.parametersSubscription.unsubscribe();

    // On nettoie la boucle mettant à jour les compteurs
    clearInterval(this.countInterval);
  }
}
