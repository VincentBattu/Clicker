import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Parameters, ParametersStore } from '../../../store/store/parameters-store.service';
import { CountsStore } from '../../../store/store/counts-store.service';

@Component({
  selector: 'app-ban-button',
  templateUrl: './ban-button.component.html',
  styleUrls: ['./ban-button.component.css']
})
export class BanButtonComponent implements OnInit, OnDestroy {

  /**
   * Abonnement aux changements des paramètres
   */
  private parametersSubscription: Subscription;

  /**
   * Paramètres
   */
  private parameters: Parameters;

  /**
   * Couleur de fond du bouton
   * @type {string}
   */
  public background = 'linear-gradient(#c0392b, #6d231c)';

  constructor (private countsStore: CountsStore,
               private parametersStore: ParametersStore) {
  }

  ngOnInit () {
    // On ne s'abonne pas aux changements du store de count car on n'a pas besoin des valeurs dans le composant
    this.parametersSubscription = this.parametersStore.state.subscribe(p => this.parameters = p);
  }

  ngOnDestroy () {
    this.parametersSubscription.unsubscribe();
  }

  /**
   * Au click sur le bouton ban
   */
  beforeBan () {
    this.background = 'linear-gradient(#6d231c, #c0392b)';
  }

  /**
   * Au relachement du bouton ban
   */
  banned () {
    this.background = 'linear-gradient(#c0392b, #6d231c)';
    // On rajoute un clic
    this.countsStore.addClick();
    this.countsStore.increaseClickCounts(this.parameters.perClick);
  }
}
