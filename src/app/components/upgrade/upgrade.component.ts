import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import Big from 'big.js';
import { Upgrade } from '../../../model/upgrades/upgrade';
import { Currency } from '../../../model/upgrades/currency';
import { UpgradeType } from '../../../model/upgrades/upgrade-type';
import { UpgradeScope } from '../../../model/upgrades/upgrade-scope';
import { Subscription } from 'rxjs';
import { Parameters, ParametersStore } from '../../../store/store/parameters-store.service';
import { ProducersStore } from '../../../store/store/producers-store.service';
import { UpgradesStore } from '../../../store/store/upgrades-store.service';
import { CountsStore } from '../../../store/store/counts-store.service';

@Component({
  selector: 'app-upgrade',
  templateUrl: './upgrade.component.html',
  styleUrls: ['./upgrade.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpgradeComponent implements OnInit, OnDestroy {

  /**
   * Souscription aux changements du store des paramètres
   */
  private parametersSubscription: Subscription;

  /**
   * Amélioration, passée par le composant parent
   */
  @Input()
  public upgrade: Upgrade;

  /**
   * Paramètres partagés
   */
  public parameters: Parameters;


  constructor (private parametersStore: ParametersStore,
               private producersStore: ProducersStore,
               private upgradesStore: UpgradesStore,
               private countsStore: CountsStore,
               private cdr: ChangeDetectorRef) {
  }

  ngOnInit () {
    // On souscrit aux changements des stores
    this.parametersSubscription = this.parametersStore.state.subscribe(p => {
      this.parameters = p;
      this.cdr.detectChanges();
    });
  }

  ngOnDestroy () {
    // On se désabonne aux changements des stores
    this.parametersSubscription.unsubscribe();
  }

  /**
   * A l'achat de l'amélioration
   */
  public buy () {

    // Si l'amélioration ne peut pas être achetée, on s'arrête
    if (!this.isPurchasable()) {
      return;
    }

    this.applyEffect();
    this.upgrade.unlocked = true;

    this.countsStore.decreaseCount(new Big(this.upgrade.price));
    this.upgradesStore.update(this.upgrade);
  }

  /**
   * L'amélioration peut-elle être achetée
   * @returns {boolean}
   */
  private isPurchasable (): boolean {
    if (this.upgrade.priceCurrency === Currency.Ban) {
      return this.countsStore.value.currentCount.gte(this.upgrade.price);
    }
  }

  /**
   * Applique les effets de l'amélioration
   */
  private applyEffect (): void {

    const upgradeScope = this.upgrade.scope;

    console.log(this.upgrade);
    if (upgradeScope === UpgradeScope.Producer) {
      this.applyProducerEffects();
    } else if (upgradeScope === UpgradeScope.Global) {
      this.applyGlobalEffects();
    } else if (upgradeScope === UpgradeScope.Click) {
      this.applyClickEffect();
    }
  }

  /**
   * Effets appliqués pour les améliorations propres aux producteurs
   */
  private applyProducerEffects () {

    const upgradeType = this.upgrade.type;

    if (upgradeType === UpgradeType.IncreaseFlat) {
      const producer = this.producersStore.value[this.upgrade.producerId];
      producer.production = producer.production.mul(this.upgrade.increase);

      this.producersStore.update(producer);
      this.parametersStore.computePerSecond(this.producersStore.value);

    } else if (upgradeType === UpgradeType.DecreaseCost) {
      const producer = this.producersStore.value[this.upgrade.producerId];
      producer.decreaseCost = this.upgrade.increase;

      this.producersStore.update(producer);
      this.parametersStore.computePerSecond(this.producersStore.value);

    } else if (upgradeType === UpgradeType.IncreaseScaling) {
      const producer = this.producersStore.value[this.upgrade.producerId];

      this.parametersStore.setAddModeratorProd(this.upgrade.increase);
      this.producersStore.update(producer);

      this.parametersStore.computePerSecond(this.producersStore.value);
    }
  }

  /**
   * Effets appliqués pour les améliorations globales
   */
  private applyGlobalEffects () {
    if (this.upgrade.type === UpgradeType.GameName) {
      this.parametersStore.setChangeGameName(true);
    }
  }

  /**
   * Effets appliqués pour les améliorations relatives aux clics
   */
  private applyClickEffect () {
    if (this.upgrade.type === UpgradeType.IncreaseFlat) {
      this.parametersStore.setPerClick(this.parameters.perClick.mul(this.upgrade.increase));
    }
  }

}
