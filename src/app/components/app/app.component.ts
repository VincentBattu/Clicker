import { Component, OnDestroy, OnInit } from '@angular/core';
import { PRODUCERS } from '../../data/producers';
import { UPGRADES } from '../../data/upgrades';
import { Subscription } from 'rxjs';
import { Parameters, ParametersStore } from '../../../store/store/parameters-store.service';
import { ProducersStore } from '../../../store/store/producers-store.service';
import { UpgradesStore } from '../../../store/store/upgrades-store.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  /**
   * Souscription aux changements des paramètres
   */
  private parametersSubscription: Subscription;

  /**
   * Paramètres partagés
   */
  public parameters: Parameters;

  /**
   * Affichage de la page de paramètres
   */
  public showParameters = false;

  /**
   * Affichage du patch note
   */
  public showPatchNote = false;

  /**
   * Affichage des succès
   */
  public showSuccess = false;

  /**
   * Numéro de version
   * @type {string}
   */
  public version = '1.1.1';


  public constructor (public parametersStore: ParametersStore,
                      private producersStore: ProducersStore,
                      private upgradeStore: UpgradesStore) {
  }

  ngOnInit (): void {
    // On souscrit aux changements des stores
    this.parametersSubscription = this.parametersStore.state.subscribe(p => this.parameters = p);

    // On initialise les producteurs et les améliorations
    this.producersStore.init(PRODUCERS);

    this.upgradeStore.init(UPGRADES);
  }

  ngOnDestroy () {
    // On se désabonne aux changements des stores
    this.parametersSubscription.unsubscribe();
  }

  public displayParameters (e: MouseEvent) {
    e.preventDefault();
    this.showParameters = true;
  }

  public hideParameters () {
    this.showParameters = false;
  }

  public displayPatchNote (e: MouseEvent) {
    e.preventDefault();
    this.showPatchNote = true;
  }

  public hidePatchNote () {
    this.showPatchNote = false;
  }

  public displaySuccess (e: MouseEvent) {
    e.preventDefault();
    this.showSuccess = true;
  }

  public hideSuccess () {
    this.showSuccess = false;
  }

}
