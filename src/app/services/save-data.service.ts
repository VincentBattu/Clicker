import { Injectable } from '@angular/core';
import { AppModule } from '../app.module';
import Big from 'big.js';

@Injectable({
  providedIn: AppModule
})
export class SaveDataService {

  private static readonly COUNT_LOCAL_STORAGE = 'count';

  public saveCount (count: Big): void {
    localStorage.setItem(count.toString(), SaveDataService.COUNT_LOCAL_STORAGE);
  }

  public loadCount (): Big {
    return new Big(localStorage.getItem(SaveDataService.COUNT_LOCAL_STORAGE));
  }
}
