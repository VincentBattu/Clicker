import { Pipe, PipeTransform } from '@angular/core';
import { Currency } from '../../model/upgrades/currency';

@Pipe({ name: 'currency' })
export class FormatCurrencyPipe implements PipeTransform {

  transform (currency: Currency, ...args: any[]): any {
    if (currency === Currency.Ban) {
      return 'ban';
    }
    return '';
  }

}
