import { Inject, LOCALE_ID, Pipe, PipeTransform } from '@angular/core';
import { Language } from '../../model/language';

/**
 * Permet de renvoyer la valeur correspondant à la langue actuelle, récupéré à l'aide de la constante
 * <code>LOCALE_ID</code>, dans un objet de type {@link Language}
 */
@Pipe({ name: 'translate' })
export class TranslatePipe implements PipeTransform {

  constructor (@Inject(LOCALE_ID) private locale: string) {
  }

  transform (language: Language): any {
    switch (this.locale) {
      case 'fr-FR':
        return language.fr;
      default:
        return language.fr;
    }
  }

}
