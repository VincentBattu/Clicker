import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'bigNumber' })
export class FormatBigNumberPipe implements PipeTransform {

  transform (value: any, ...args: any[]): any {
    if (typeof value === 'string') {
      return value.replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
    }
    if (typeof value === 'number') {
      return (value as number).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
    }
    return '';
  }

}
